import pyopencl as cl
import numpy as np
from jinja2 import Template
import cliargs
import kernels.opencl_kernels as kernels


def pad(total, pad):
    return pad * ((total-1) / pad + 1)

args, options = cliargs.get_options()

point_count = args.point_count
mean_count = args.mean_count
dim_count = args.dim_count
workgroup_size = args.workgroup_size

workgroup_count = pad(point_count, workgroup_size) / workgroup_size

assert workgroup_size == 2**np.log2(workgroup_size), "Workgroup size must be a power of two"

values = np.random.rand(point_count).astype(np.float32)

ctx = args.ctx
queue = cl.CommandQueue(ctx)

mf = cl.mem_flags
min_buf = cl.Buffer(ctx, mf.READ_WRITE, 4)
print "values size: %.2f MB" % (values.nbytes / 1024.0 / 1024.0)
values_buf = cl.Buffer(ctx, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=values)


template_vars = {
    'point_count': point_count,
    'reduction_op': 'fmin',
    'neutral_element': 'HUGE_VALF',
    'workgroup_size0': 'get_local_size(0)',
}

min_kernel = cl.Program(ctx,
                        str(Template(kernels.reduce_code).render(template_vars))
             ).build()

value_count = point_count
workgroup_count = pad(value_count, workgroup_size) / workgroup_size
reduced_vals_buf = cl.Buffer(ctx, mf.READ_WRITE, 4 * workgroup_count)
while value_count > 1:
    print "Calling kernel"
    min_kernel.reduce(
        queue,
        (pad(value_count, workgroup_size), 1),
        (workgroup_size, 1),
        values_buf,
        reduced_vals_buf,
        cl.LocalMemory(4 * workgroup_size),
        np.uint32(value_count),
    ).wait()
    workgroup_count = pad(value_count, workgroup_size) / workgroup_size
    value_count = workgroup_count
    values_buf = reduced_vals_buf

np_min = np.min(values)

#print values

reduced_vals = np.zeros((1,)).astype(np.float32)
cl.enqueue_copy(queue, reduced_vals, reduced_vals_buf).wait()

#print values[0:workgroup_size]
#print values

#print np.min(values[0:workgroup_count]), np_min
#if np.min(values[0:workgroup_count]) == np_min:
print reduced_vals, np_min
if reduced_vals == np_min:
    print "Same"
else:
    print "Error"
