import os

options = {}

if os.environ.get('USE_CUDA'):
    import pycuda.driver as cuda
    from pycuda.compiler import SourceModule
    import pycuda.gpuarray as array
    options['cuda'] = True
else:
    import pyopencl as cl
    import pyopencl.array as array
    options['opencl'] = True

import numpy as np
import numpy
import random
import time

import cliargs
import kmeans_opencl
from kmeans_opencl import pad

code = """
$kernel void dists_simple($global const float *X,
                          $global const float *m,
                          $global float *dists)
{
  //# return;
  unsigned point_id = $global_id0;
  unsigned mean_id = $global_id1;

  if (point_id < $point_count && mean_id < $mean_count) {
    float dist = 0.0f;
#if not $dimLoopUnroll
    for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
    float temp;
#for i_dim in $dims
      temp = X[point_id * $dim_count + $i_dim] - m[mean_id * $dim_count + $i_dim];
      temp *= temp;
      dist += temp;
#end for
#if not $dimLoopUnroll
    }
#end if
    // dists[point_id * $mean_count + mean_id] = dist;
    dists[mean_id * $point_count + point_id] = dist;
  }
}

__kernel void dists_simple_layout(__global const float *X,
                                  __global const float *m,
                                  __global float *dists)
{
  unsigned point_id = $global_id0;
  unsigned mean_id = $global_id1;

  if (point_id < $point_count && mean_id < $mean_count) {
    float dist = 0.0f;
#if $dimLoopUnroll
    float temp;
#for i_dim in $dims
      temp = X[$point_count * $i_dim + point_id] - m[$mean_count * $i_dim + mean_id];
      temp *= temp;
      dist += temp;
#end for
#else
    for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
      float temp = X[$point_count * $i_dim + point_id] - m[$mean_count * $i_dim + mean_id];
      temp *= temp;
      dist += temp;
    }
#end if
    // dists[point_id * $mean_count + mean_id] = dist;
    dists[mean_id * $point_count + point_id] = dist;
  }
}
"""

# Lloyd's algorithm
def kmeans(points, means, options={}, empty_cluster_method=4):

    ctx = None
    queue = None
    device = None

    global_mem_size = 0
    local_mem_size = 0
    max_workgroup_size = 0

    if 'opencl' in options and options['opencl']:
        if 'ctx' not in options or options['ctx'] is None:
            ctx = cl.create_some_context()
        else:
            ctx = options['ctx']

        properties = 0
        if 'profiling' in options and options['profiling']:
            properties |= cl.command_queue_properties.PROFILING_ENABLE

        if 'out_of_order' in options and options['out_of_order']:
            properties |= cl.command_queue_properties.OUT_OF_ORDER_EXEC_MODE_ENABLE

        queue = cl.CommandQueue(ctx, properties=properties)
        device = ctx.devices[0]

        global_mem_size = ctx.devices[0].global_mem_size
        local_mem_size = ctx.devices[0].local_mem_size
        max_workgroup_size = ctx.devices[0].max_work_item_sizes[0]

    elif 'cuda' in options and options['cuda']:
        import pycuda.autoinit
        ctx = pycuda.autoinit.context
        device = pycuda.autoinit.device

        if 'profiling' in options and options['profiling']:
            pycuda.driver.initialize_profiler(config_file, output_file, output_mode)
            pycuda.driver.start_profiler()

        global_mem_size = device.total_memory()
        local_mem_size = pycuda.tools.DeviceData(device).shared_memory
        max_workgroup_size = pycuda.tools.DeviceData(device).max_threads

        print 'Compute capability:', device.compute_capability()
        options['compute_capability'] = device.compute_capability()
        print 'Warp size:', pycuda.tools.DeviceData(device).warp_size

    if 'verbose' in options and options['verbose']:
        print 'Global memory:', global_mem_size / 1024.0 / 1024.0, 'MiB'
        print 'Local memory:', local_mem_size / 1024.0, 'KiB'

    if 'print_input' in options and options['print_input']:
        print 'points:'
        print points
        print 'means:'
        print means

    point_count, dim_count = points.shape
    mean_count, dim_count2 = means.shape
    assert(dim_count == dim_count2)
    kmeans_opencl.setup_keywords(options)
    # queue = options.queue
    options['template_keywords'].update(options)

    X = points
    M = means
    n = point_count
    k = mean_count
    d = dim_count
    Xbuf = kmeans_opencl.allocate_buffer(ctx, queue, X.T.astype(np.float32), name='X')
    Mbuf = kmeans_opencl.allocate_buffer(ctx, queue, M.T.astype(np.float32), name='M')
    XTbuf = kmeans_opencl.allocate_buffer(ctx, queue, X.astype(np.float32), name='X')
    MTbuf = kmeans_opencl.allocate_buffer(ctx, queue, M.astype(np.float32), name='M')
    D0buf = kmeans_opencl.allocate_buffer(ctx, queue, 4*n*k, name='D')
    D1buf = kmeans_opencl.allocate_buffer(ctx, queue, 4*n*k, name='D')

    # print options['template_keywords']['dims']
    print 'Loop unroll:', options['template_keywords']['dimLoopUnroll']

    options['compile_time'] = 0
    print "Compiling..."
    kernel_simple = kmeans_opencl.compile_kernel(ctx, code, 'dists_simple', options['template_keywords'], options)
    kernel_simple_layout = kmeans_opencl.compile_kernel(ctx, code, 'dists_simple_layout', options['template_keywords'], options)
    print "Done"

    # start = time.time()
    # kmeans_opencl.call_kernel_imp(ctx, kernel, [[n,k],[1,1]], [Xbuf, Mbuf, Dbuf], [], options)

    # if 'opencl' in options and options['opencl']:
    #     calc_delta_event.wait()
    # elif 'cuda' in options and options['cuda']:
    #     pycuda.driver.Context.synchronize()
    # end = time.time()
    # print "Took %.2f s" % (end - start)

    tot_time = 0
    mat_time = 0

    times = [[], []]
    kmeans_opencl.transfer_memory(queue, Mbuf, M).wait()
    kmeans_opencl.transfer_memory(queue, MTbuf, np.asfortranarray(M)).wait()

    for i in xrange(10):
        print i
        start = time.time()
        event = kmeans_opencl.call_kernel_imp(queue, kernel_simple, [[pad(n, 16),pad(k, 16)],[16,16]], [Xbuf, Mbuf, D0buf], [], options)
        if 'opencl' in options and options['opencl']:
            event.wait()
        elif 'cuda' in options and options['cuda']:
            pycuda.driver.Context.synchronize()
        stop = time.time()
        times[0].append(stop - start)
        start = time.time()
        event = kmeans_opencl.call_kernel_imp(queue, kernel_simple_layout, [[pad(n, 16),pad(k, 16)],[16,16]], [XTbuf, MTbuf, D1buf], [], options)
        if 'opencl' in options and options['opencl']:
            event.wait()
        elif 'cuda' in options and options['cuda']:
            pycuda.driver.Context.synchronize()
        stop = time.time()
        times[1].append(stop - start)

        ### Check equality
        # D0 = np.ndarray((n,k)).astype(np.float32)
        # D1 = np.ndarray((n,k)).astype(np.float32)
        # kmeans_opencl.transfer_memory(queue, D0, D0buf).wait()
        # kmeans_opencl.transfer_memory(queue, D1, D1buf).wait()
        # assert np.allclose(D0,D1)

        ### Numpy
        # D = D2
        # Calculate distances (squared)
        # D = -2 * X.dot(M.T)
        # D += np.reshape(np.sum(X**2, 1), (n, 1))
        # D += np.reshape(np.sum(M**2, 1), (1, k))
        # print D.shape
        # print D0.shape
        # print D1.shape
        # print np.sum(D-D0)
        # assert np.allclose(D0,D)
        # exit()

    print "%.2f (%.2f) s"% (np.mean(times[0]), np.std(times[0]))
    print "%.2f (%.2f) s"% (np.mean(times[1]), np.std(times[1]))

if __name__ == "__main__":
    import cliargs
    args, options = cliargs.get_options(get_arrays=True, get_context=True)

    kmeans(args.points, args.means, options)
