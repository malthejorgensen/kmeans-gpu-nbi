import re
import numpy as np
import time
import cliargs

## Profiling options
sample_count = 10
#point_counts = [10, 50, 100] #, 200] #, 500, 1000]
point_counts = [200, 500, 1000]

numpy_method = None

import kmeans_opencl
import kmeans_numpy
# import dists_test
functions = [
    #('LBG', lambda p,m: lbg(p, len(m))),
    ('GPU', lambda p, m, o={}, weights=None: kmeans_opencl.kmeans(p, m, o, weights)),  # ('GPU', kmeans_gpu),
    ('Numpy', lambda p, m, o={}, weights=None: kmeans_numpy.kmeans(p, m, o, numpy_method)),
    # ('Dists test', lambda p, m, o={}, weights=None: dists_test.kmeans(p, m, o, numpy_method)),
    #('GPU', lambda p, m: kmeans_gpu(p, m, {'lbg': True, 'max_step_count': args.max_steps, 'max_delta': args.max_delta})),  #('GPU', kmeans_gpu),
    #('GPU /w CPU stepping', lambda p,m: kmeans_gpu_cpu_step(p, m)),
]

try:
    import kmeans_fortran
    functions += [
        ('Fortran', lambda p, m, o={}, weights=None: kmeans_fortran.kmeans(p, m, o, weights=weights))
    ]
except ImportError:
    print "Fortran libraries not found"

try:
    import scipy.cluster.vq
    functions += [
        ('Scipy kmeans2', lambda p, m, o={}, weights=None: scipy.cluster.vq.kmeans2(p, np.array(m))[0]),
        ('Scipy kmeans', lambda p, m, o={}, weights=None: scipy.cluster.vq.kmeans(p, np.array(m))[0]),
    ]
except:
    print "Scipy libraries not found"

markers = ['o', 'o', 'o', 'o']
colors = ['red', 'green', 'blue', 'black']

# Use Linde-Buzo-Gray?
use_lbg = True
desired_means = 128


def kmeans_gpu_cpu_step(X, m):
    for i in xrange(10000):
        m_new = kmeans_opencl.kmeans(X, m, {'max_step_count': 1})

        #if np.max(m_new - np.delete(m, delete_rows, axis=0)) < 0.00001:
        if np.max(m_new - m) < 0.00001:
            #print "BREAK %s" % i
            break
        else:
            m = m_new

    return m_new


def print_equal(means, name1, name2):

    # Sort means -- GPU and Fortran return the same means but in different
    #               order
    a = means[name1]
    b = means[name2]
    a = a[a[:,0].argsort()]
    b = b[b[:,0].argsort()]

    eq = '!='
    if np.array_equal(a, b):
        eq = "=="
    elif np.allclose(a, b):
        eq = "~="

    print "%s %s %s" % (name1, eq, name2)


def check_equal(m, options):
    numpy_method = options['numpy_method']
    #import warnings

    # If we use method (2): deletion of empty clusters, we expect numpy and
    # scipy.kmeans result to be equal
    if numpy_method == 2 and 'Numpy' in m and 'Scipy kmeans' in m:
        print_equal(m, 'Numpy', 'Scipy kmeans')

    # If we use method (4): no action on empty clusters, we expect numpy and
    # scipy.kmeans result to be equal
    if numpy_method == 4 and 'Numpy' in m and 'Scipy kmeans2' in m:
        print_equal(m, 'Numpy', 'Scipy kmeans2')

    if numpy_method == 4 and 'Numpy' in m and 'GPU' in m:
        print_equal(m, 'Numpy', 'GPU')

    if numpy_method == 4 and 'Numpy' in m and 'Dists test' in m:
        print_equal(m, 'Dists test', 'Numpy')
    if numpy_method == 4 and 'GPU' in m and 'Dists test' in m:
        print_equal(m, 'Dists test', 'GPU')

    if 'Fortran' in m and 'GPU' in m:
        print_equal(m, 'Fortran', 'GPU')

    if 'Fortran' in m and 'Numpy' in m:
        print_equal(m, 'Fortran', 'Numpy')

    #if not np.array_equal(m['Scipy kmeans'], m['Scipy kmeans2']):
        #warnings.warn("Scipy kmeans and Scipy kmeans2 solution not equal")
        #print "Scipy kmeans and Scipy kmeans2 solution not equal"

    # GPU and kmeans2 let empty clusters stay where they are
    # they should in principle get the same results
    if 'GPU' in m and 'Scipy kmeans2' in m:
        print_equal(m, 'GPU', 'Scipy kmeans2')

            #print m['GPU'] - m['Scipy kmeans2']
            #print m['GPU']
            #print m['Scipy kmeans2']

    if 'LBG' in options and options['LBG']:
        print_equal(m, 'GPU', 'Fortran')


# If module is run as program
if __name__ == '__main__':
    np.seterr(all='raise')

    args, options = cliargs.get_options(get_context=True)

    numpy_method = args.numpy_method
    if args.no_numpy:
        numpy_method = 0

    dim_count = args.dim_count
    if args.visual:
        import plot

    mean_count = args.mean_count
    point_count = args.point_count

    #print 'Workgroup size:', workgroup_size

    if args.fortran:
        points = np.loadtxt('fortran/6d_data.txt')
        points = points[:, 0:6]  # We want the first six columns
        #points = points[:, 0:2]  # The first three columns are (x,y,z)
        dim_count = 6
        print points
        desired_means = 128
        means = np.zeros((desired_means, dim_count))

        functions = [('GPU', lambda p, f: (p, f))]
        for name, f in functions:
            start = time.time()
            #lbg(points, desired_means, f)
            kmeans_gpu(points, means, {'lbg': True, 'max_step_count': args.max_steps, 'max_delta': args.max_delta,
                                       'workgroup_size': 16})  #('GPU', kmeans_gpu),
            perf_time = time.time() - start

            print '%s: %0.2f s' % (name, perf_time)

    elif args.profiling and False:
        time_means = []
        time_stds = []
        for i in xrange(len(functions)):
            time_means.append([])
            time_stds.append([])

        for point_count in point_counts:

            times = []
            for i in xrange(len(functions)):
                times.append(np.zeros(sample_count))

            points = []
            means = []
            mean_count = (0.3 * point_count) // 1

            for run in xrange(sample_count):
                points.append(np.mat(np.random.rand(point_count, dim_count)))
                means.append(np.mat(np.random.rand(mean_count, dim_count)))

            for f_idx, (name, f) in zip(xrange(len(functions)), functions):

                print "Running %s %s times on %s points..." % (name, sample_count, point_count)
                for run in xrange(sample_count):
                    start = time.time()
                    f(points[run], means[run], options)
                    times[f_idx][run] = time.time() - start

            for i in xrange(len(functions)):
                time_means[i].append(times[i].mean())
                time_stds[i].append(times[i].std())

        import matplotlib.pyplot as plt
        for i in xrange(len(functions)):
            # http://scienceoss.com/errorbars-in-matplotlib/
            plt.errorbar(point_counts, time_means[i], yerr=time_stds[i], label=functions[i][0], linestyle='None', marker=markers[i], markerfacecolor=colors[i], ecolor=colors[i], markersize=5)

        plt.xlabel('# points')
        plt.ylabel('run time (s)')
        plt.xlim(0, point_counts[-1] * 1.1)
        plt.legend(loc='upper left')  # http://matplotlib.org/users/legend_guide.html
        plt.show()

    # Visually show results of the different methods (Numpy, GPU, Scipy etc.)
    else:
        #points = np.mat(np.random.rand(point_count, dim_count))
        #means = np.mat(np.random.rand(mean_count, dim_count))
        points = np.random.rand(point_count, dim_count).astype(np.float32)
        means = np.random.rand(mean_count, dim_count).astype(np.float32)

        weights = None
        if args.weighted or args.log_weighted:
            weights = np.random.rand(point_count, 1)
            weights = np.ones((point_count, 1))
            weights[5:] *= 0.001

            functions = filter(lambda t: 'GPU' in t[0] or 'Fortran' in t[0], functions)

        if args.visual:
            import matplotlib.pyplot as plt
            plt.figure(figsize=(15.5, 5.5))

        m = {}
        #p = scipy.cluster.vq.whiten(points)  # This is recommended

        if ('weighted' in options and options['weighted']) or \
           ('log-weighted' in options and options['log-weighted']):
            functions = [p for p in functions if p[0] != 'Scipy kmeans']
            functions = [p for p in functions if p[0] != 'Scipy kmeans2']

        if 'no_numpy' in options and options['no_numpy']:
            functions = [p for p in functions if p[0] != 'Numpy']

        print options
        for i, (name, f) in enumerate(functions):

            import sys
            print 'Calculating %s...' % name,
            sys.stdout.flush()
            if args.LBG and not ('GPU' in name or 'Fortran' in name):
                continue
                m[name] = kmeans_numpy.lbg(points, means.shape[0], f)
            else:
                m[name] = f(points, means, options, weights=weights)
            print 'Done'


            if args.visual:
                a = plt.subplot(100 + 10 * len(functions) + i + 1)
                a.set_title(name)
                plot.plotFull(points, m[name], show=False, axes=a)

        print 'Checking equality...'
        check_equal(m, options)
        print 'Done'

        if args.visual:
            plt.show(block=True)
