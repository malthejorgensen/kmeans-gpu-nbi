# coding=utf8
import pyopencl as cl
import numpy as np

from jinja2 import Template

import os, sys
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
import kernels.opencl_kernels as kernels
import cliargs

import event_history as eh
event_history = eh.EventHistory()


def pad(total, unit):
    return unit * ((total - 1) // unit + 1)


if __name__ == '__main__':
    args, options = cliargs.get_options()
    ctx = args.ctx
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # Generate test sets
    point_count = options['point_count']
    mean_count = options['mean_count']
    dim_count = options['dim_count']
    #sample_count = options['sample_count']

    dists = np.random.rand(point_count, mean_count).astype(np.float32)
    idxs = np.tile(np.arange(mean_count), (point_count, 1)).astype(np.uint32)

    mf = cl.mem_flags
    dists_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, dists.nbytes, hostbuf=dists)
    idxs_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, idxs.nbytes, hostbuf=idxs)

    max_workgroup_size = ctx.devices[0].max_work_item_sizes[0]

    # Reduction code
    dists_min_kernel = cl.Program(ctx,
                                str(Template(kernels.dists_min_code).render())
                                ).build()

    value_count = mean_count
    workgroup_count = pad(value_count, max_workgroup_size) / max_workgroup_size

    values_buf = dists_buf
    reduced_vals_buf = cl.Buffer(ctx, mf.READ_WRITE, 4 * workgroup_count * point_count)
    reduced_vals_idxs_buf = cl.Buffer(ctx, mf.READ_WRITE, 4 * workgroup_count * point_count)

    reduce_time = 0

    while value_count > 1:
        reduce_event = dists_min_kernel.dists_min(
            queue,
            (pad(value_count, max_workgroup_size), point_count),
            (max_workgroup_size, 1),
            values_buf,
            idxs_buf,
            reduced_vals_buf,
            reduced_vals_idxs_buf,
            cl.LocalMemory(4 * max_workgroup_size),
            cl.LocalMemory(4 * max_workgroup_size),
            np.int32(value_count),
            #wait_for=[log_event]
        )
        workgroup_count = pad(value_count, max_workgroup_size) / max_workgroup_size
        value_count = workgroup_count
        values_buf = reduced_vals_buf
        idxs_buf = reduced_vals_idxs_buf
        reduce_event.wait()

        event_history.log('reduce_event', reduce_event)

        reduce_time += ((reduce_event.profile.end - reduce_event.profile.start) / 10.0**9)

    print 'Reduce time: %.2f s' % reduce_time

    reduced_vals = np.zeros((point_count,)).astype(np.float32)
    reduce_vals_mem_event = cl.enqueue_copy(queue, reduced_vals, reduced_vals_buf)
    reduce_vals_mem_event.wait()
    event_history.log('reduce_vals_mem_event', reduce_vals_mem_event)

    reduced_vals_idxs = np.zeros((point_count,)).astype(np.uint32)
    reduced_vals_idxs_mem_event = cl.enqueue_copy(queue, reduced_vals_idxs, reduced_vals_idxs_buf)
    reduced_vals_idxs_mem_event.wait()
    event_history.log('reduced_vals_idxs_mem_event', reduced_vals_idxs_mem_event)

    template_var_dict = {
        #'workgroup_size': workgroup_size,
        'workgroup_size0': 'get_local_size(0)',
        'workgroup_size1': 'get_local_size(1)',
        #'step_size': step_size,
        'point_count': point_count,
        'mean_count': mean_count,
        #'dim_count': dim_count,

        'max_delta': 1,
    }
    current_mean_count = mean_count
    min1p_kernel = cl.Program(ctx, str(Template(kernels.minimum_first_pass_code).render(template_var_dict))).build()
    min2p_kernel = cl.Program(ctx, str(Template(kernels.minimum_second_pass_code).render(template_var_dict))).build()

    workgroup_size_mins = 128
    mins_buf_size = 4 * point_count * workgroup_size_mins
    print 'Allocating "mins_buf": %.2f MiB' % (mins_buf_size / 1024.0**2)
    mins_buf     = cl.Buffer(ctx, mf.READ_WRITE, mins_buf_size)
    min_idxs_buf = cl.Buffer(ctx, mf.READ_WRITE, mins_buf_size)

    is_delta = np.zeros((1,)).astype(np.float32)
    is_delta[0] = np.float32(np.inf)
    is_delta[0] = np.inf
    is_delta_buf = cl.Buffer(ctx, mf.READ_WRITE, is_delta.nbytes)
    delta_mem_copy_event = cl.enqueue_copy(queue, is_delta_buf, is_delta)
    delta_mem_copy_event.wait()
    event_history.log('delta_mem_copy_event', delta_mem_copy_event)

    min1p_event = min1p_kernel.mins(
        queue,
        (point_count, workgroup_size_mins)[::-1],  #wavefront_size), # Global size
        (1, workgroup_size_mins)[::-1],  #None, #(1, wavefront_size), # Local size (workgroup size)
        #Kernel arguments
        dists_buf,
        mins_buf,
        min_idxs_buf,
        np.uint32(current_mean_count),
        is_delta_buf,
    )

    min2p_event = min2p_kernel.mins(
        queue,
        (pad(point_count, workgroup_size_mins), 1),  # Global size
        (workgroup_size_mins, 1),  #None,  #(wavefront_size, 1), # Local size (workgroup size)
        #Kernel arguments
        mins_buf,
        min_idxs_buf,
        is_delta_buf,
        wait_for=[min1p_event]
    )
    min2p_event.wait()

    event_history.log('min1p_event', min1p_event)
    event_history.log('min2p_event', min2p_event)


    min_time = ((min1p_event.profile.end - min1p_event.profile.start) / 10.0**9)
    min_time += ((min2p_event.profile.end - min2p_event.profile.start) / 10.0**9)

    print 'Min time: %.2f s' % min_time

    min_vals = np.zeros((point_count, workgroup_size_mins)).astype(np.float32)
    min_vals_mem_event = cl.enqueue_copy(queue, min_vals, mins_buf)
    min_vals_mem_event.wait()

    min_vals_idxs = np.zeros((point_count, workgroup_size_mins)).astype(np.uint32)
    min_vals_idxs_mem_event = cl.enqueue_copy(queue, min_vals_idxs, min_idxs_buf)
    min_vals_idxs_mem_event.wait()
    event_history.log('min_vals_mem_event', min_vals_mem_event)
    event_history.log('min_vals_idxs_mem_event', min_vals_idxs_mem_event)

    print 'Values equal:', np.array_equal(reduced_vals, min_vals[:, 0])
    print 'Values close:', np.allclose(reduced_vals, min_vals[:, 0])
    print 'Indexes equal:', np.array_equal(reduced_vals_idxs, min_vals_idxs[:, 0])

    event_history.graph()
    #print reduced_vals_idxs
    #print min_vals_idxs[:, 0]

    #for i, d in enumerate(min_vals_idxs[:, 0]):
        #print min_vals[i, 0], dists[i, d]

    #for i, d in enumerate(reduced_vals_idxs):
        #print reduced_vals[i], dists[i, d]
    #print min_vals_idxs
    #print reduced_vals_idxs
    #print min_vals_idxs
