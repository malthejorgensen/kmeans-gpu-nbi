# coding=utf8
import numpy as np
import pyopencl as cl
from jinja2 import Template

import os, sys
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
import kernels.opencl_kernels as kernels
import cliargs


def pad(total, unit):
    return unit * ((total - 1) // unit + 1)


if __name__ == '__main__':
    args, options = cliargs.get_options()
    ctx = args.ctx
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # Generate test sets
    point_count = options['point_count']
    mean_count = options['mean_count']
    dim_count = options['dim_count']
    #sample_count = options['sample_count']

    points = np.random.rand(point_count, dim_count)
    means = np.random.rand(mean_count, dim_count)

    # Transfer memory (is actually delayed until memory is read/written in a kernel)
    mf = cl.mem_flags
    X_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=points.astype(np.float32))
    m_buf = cl.Buffer(ctx, mf.COPY_HOST_PTR, hostbuf=means.astype(np.float32))
    X_T_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=np.asfortranarray(points.astype(np.float32)))
    m_T_buf = cl.Buffer(ctx, mf.COPY_HOST_PTR, hostbuf=np.asfortranarray(means.astype(np.float32)))
    dists_buf = cl.Buffer(ctx, mf.WRITE_ONLY, 4 * point_count * mean_count)

    is_delta = np.zeros((1,)).astype(np.float32)
    is_delta_buf = cl.Buffer(ctx, mf.READ_WRITE | mf.COPY_HOST_PTR, is_delta.nbytes, hostbuf=is_delta)

    settings = {
        'point_count': point_count,
        'mean_count': mean_count,
        'dim_count': dim_count,
        'dims': range(dim_count),
        'workgroup_size0': 'get_local_size(0)',
        'workgroup_size1': 'get_local_size(1)',
        'workgroup_size': 'AAA',
        'max_delta': 0,
    }
    loop_unroll = True
    if loop_unroll:
        settings['dimLoopUnroll'] = True
    else:
        settings['dimLoopUnroll'] = False
        settings['i_dim'] = 'i_dim'
    settings.update(options)

    kernel_list = [
        # (Name, function name)
        ('Naive', 'dists_simple'),  # Naïve
        ('Naive transposed', 'dists_simple_layout'),
        ('Private memory', 'dists'),
        ('Local memory', 'dists_local'),
        ('Local memory transposed', 'dists_local_dim'),
    ]

    # Build kernel
    #print kernels.dists_code
    #print str(Template(kernels.dists_code).render(settings))
    dists_kernel = cl.Program(ctx, str(Template(kernels.dists_code).render(settings))).build()

    # Maximum number of workitems in a workgroup
    print("Device max work group size:", ctx.devices[0].max_work_group_size)
    print("Device max work group dimension sizes:", ctx.devices[0].max_work_group_size)

    max_workgroup_size = ctx.devices[0].max_work_group_size
    max_workgroup_dimension_size = ctx.devices[0].max_work_item_sizes

    current_size = max_workgroup_dimension_size
    d0, d1 = [], []
    while current_size[0] >= 1 or current_size[1] >= 1:
        if current_size[0] >= 1:
            d0.append((current_size[0], 1))
            current_size[0] //= 2
        if current_size[1] >= 1:
            d1.append((1, current_size[1]))
            current_size[1] //= 2

    workgroup_sizes = [None] + d0 + d1 + [
        (23, 23),
        (256, 2),
        (128, 4),
        (2, 256),
        (3, 170),
        (4, 128),
        (5, 102),
        (8, 64),
        (22, 22),
        (16, 16),
        (8, 8),
        (4, 4),
        (2, 2),
    ]
    workgroup_times = []

    print 'Workgroup size',
    for (name, fname) in kernel_list:
        print '%24s' % name,
    print ''

    for workgroup_size in workgroup_sizes:

        if workgroup_size is not None:
            global_size = (pad(point_count, workgroup_size[0]), pad(mean_count, workgroup_size[1]))
        else:
            global_size = (point_count, mean_count)

        print '%14s' % (workgroup_size,),

        for (name, fname) in kernel_list:
            global_size_adjusted = global_size
            if name == 'Private memory':
                if workgroup_size is None or workgroup_size[1] != 1:
                    print ' '*23 + '-',
                    continue
                else:
                    global_size_adjusted = (
                        workgroup_size[0] * ((point_count - 1) / workgroup_size[0] + 1),
                        1
                    )

            args = [X_buf, m_buf, dists_buf]
            if 'transposed' in name:
                args = [X_T_buf, m_T_buf, dists_buf]

            if 'Local' in name:
                if (workgroup_size is None or workgroup_size[0] != 1):
                    print ' '*23 + '-',
                    continue
                else:
                    global_size_adjusted = (
                        (point_count - 1) / workgroup_size[1] + 1,
                        workgroup_size[1] * ((mean_count - 1) / workgroup_size[1] + 1)
                    )

                    args.append(cl.LocalMemory(4 * dim_count * workgroup_size[1]))

            if name == 'Local memory transposed':
                args.append(np.uint32(mean_count))
                args.append(is_delta_buf)

            if workgroup_size is not None and workgroup_size[0] * workgroup_size[1] > max_workgroup_size:
                    print ' '*17 + 'Too big',
                    continue

            if 'verbose' in options and options['verbose']:
                print '%13s' % (global_size_adjusted, ),

            event = cl.Kernel(dists_kernel, fname)(
                queue,
                global_size_adjusted,  # Global size
                workgroup_size,  # Local size (workgroup size)
                # Kernel arguments
                *args
            )
            event.wait()
            dists_time = (event.profile.end - event.profile.start) / 10.0**9
            if 'verbose' in options and options['verbose']:
                print '%10.2f' % dists_time,
            else:
                print '%24.2f' % dists_time,

            if 'check_result' in options and options['check_result']:
                dists = np.zeros((point_count, mean_count)).astype(np.float32)
                cl.enqueue_copy(queue, dists, dists_buf).wait()
                try:
                    #if np.array_equal(dists, dists_last):
                    if not np.allclose(dists, dists_last):
                        print 'E',
                        print dists
                        print dists_last
                        continue
                except NameError:
                    #print 'D',
                    pass
                dists_last = dists.copy()

        print ''

        #workgroup_times.append((dists_event.profile.end - dists_event.profile.start) / 10.0**9)
