# coding=utf8

from gpu_lib import *

import numpy as np
import time

wavefront_size = None
workgroup_size = None
#cldevice.local_mem_size
local_mem_size = None


# Load and create kernels
import kernels.opencl_kernels as kernels_module

module_options = {}
default_max_step_count = 10000
default_max_delta = 1e-6



def log_weights(ctx, queue, point_count, points_weights_buf, weights, max_workgroup_size, template_var_dict):
    # Take logarithm of all weights
    log_kernel = compile_kernel(ctx, kernels_module.log_code, 'log_kernel', template_var_dict)
    log_event = log_kernel.maplog(
        queue,
        (pad(point_count, max_workgroup_size), 1),
        (max_workgroup_size, 1),
        points_weights_buf
    )
    event_history.log('log_event',  log_event)

    # Find minimum
    template_vars = {
        'point_count': point_count,
        'reduction_op': 'fmin',
        'neutral_element': 'NAN',
        'workgroup_size0': 'get_local_size(0)',
    }

    reduce_kernel = compile_kernel(ctx, kernels_module.reduce_code, 'reduce_kernel', template_vars)

    value_count = point_count
    workgroup_count = pad(value_count, max_workgroup_size) / max_workgroup_size

    values_buf = points_weights_buf
    mf = cl.mem_flags
    reduced_vals_buf = cl.Buffer(ctx, mf.READ_WRITE, 4 * workgroup_count)

    while value_count > 1:
        reduce_event = reduce_kernel.reduce(
            queue,
            (pad(value_count, max_workgroup_size), 1),
            (max_workgroup_size, 1),
            values_buf,
            reduced_vals_buf,
            cl.LocalMemory(4 * max_workgroup_size),
            np.int32(value_count),
            wait_for=[log_event]
        )
        event_history.log('reduce_event',  reduce_event)
        workgroup_count = pad(value_count, max_workgroup_size) / max_workgroup_size
        value_count = workgroup_count
        values_buf = reduced_vals_buf
        reduce_event.wait()

    reduced_vals = np.zeros((1,)).astype(np.float32)
    reduce_vals_mem_event = cl.enqueue_copy(queue, reduced_vals, reduced_vals_buf)
    event_history.log('reduce_vals_mem_event',  reduce_vals_mem_event)

    reduce_vals_mem_event.wait()

    print reduced_vals, np.min(np.log(weights))
    if reduced_vals == np.min(np.log(weights)):
        print "Same"
    else:
        print "Error"

    # Rescale
    rescale_kernel = compile_kernel(ctx, kernels.rescale_code, 'rescale_kernel', template_var_dict)
    rescale_event = rescale_kernel.rescale(
        queue,
        (pad(point_count, max_workgroup_size), 1),
        (max_workgroup_size, 1),
        points_weights_buf,
        reduced_vals_buf,
        wait_for=[reduce_event]
    )
    event_history.log('rescale_event',  rescale_event)
    rescale_event.wait()

    points_weights = np.zeros_like(weights).astype(np.float32)
    points_weights_mem_event = cl.enqueue_copy(queue, points_weights, points_weights_buf)
    event_history.log('points_weights_mem_event',  points_weights_mem_event)
    points_weights_mem_event.wait()
    print points_weights


def pad(total, unit):
    return unit * ((total - 1) // unit + 1)


def kmeans(points, means, options={}, weights=None):

    ctx = None
    queue = None
    device = None

    global_mem_size = 0
    local_mem_size = 0
    max_workgroup_size = 0

    if 'opencl' in options and options['opencl']:
        if 'ctx' not in options or options['ctx'] is None:
            ctx = cl.create_some_context()
        else:
            ctx = options['ctx']

        properties = 0
        if 'profiling' in options and options['profiling']:
            properties |= cl.command_queue_properties.PROFILING_ENABLE

        if 'out_of_order' in options and options['out_of_order']:
            properties |= cl.command_queue_properties.OUT_OF_ORDER_EXEC_MODE_ENABLE

        queue = cl.CommandQueue(ctx, properties=properties)

        global_mem_size = ctx.devices[0].global_mem_size
        local_mem_size = ctx.devices[0].local_mem_size
        max_workgroup_size = ctx.devices[0].max_work_item_sizes[0]

    elif 'cuda' in options and options['cuda']:
        import pycuda.autoinit
        ctx = pycuda.autoinit.context
        device = pycuda.autoinit.device

        if 'profiling' in options and options['profiling']:
            pycuda.driver.initialize_profiler(config_file, output_file, output_mode)
            pycuda.driver.start_profiler()

        global_mem_size = device.total_memory()
        local_mem_size = pycuda.tools.DeviceData(device).shared_memory
        max_workgroup_size = pycuda.tools.DeviceData(device).max_threads

        print 'Compute capability:', device.compute_capability()
        options['compute_capability'] = device.compute_capability()
        print 'Warp size:', pycuda.tools.DeviceData(device).warp_size

    if 'verbose' in options and options['verbose']:
        print 'Global memory:', global_mem_size / 1024.0 / 1024.0, 'MiB'
        print 'Local memory:', local_mem_size / 1024.0, 'KiB'

    if 'print_input' in options and options['print_input']:
        print 'points:'
        print points
        print 'means:'
        print means

    point_count, dim_count = points.shape
    mean_count, dim_count2 = means.shape
    assert(dim_count == dim_count2)

    if 'verbose' in options and options['verbose'] >= 1:
        print 'point_count', point_count
        print 'mean_count', mean_count
        print 'dim_count', dim_count

    if 'opencl' in options and options['opencl']:
        platform_name = ctx.devices[0].platform.name
        device_name = ctx.devices[0].name
        available_global_mem_size = avail_mem(global_mem_size, ctx.devices[0].platform.name, ctx.devices[0].name)
        print 'Estimated available global memory:', available_global_mem_size / 1024 / 1024, 'MiB'

    workgroup_size = options['workgroup_size']

    # My nVidia card can handle 512 workitems in each dimension of a workgroup
    # but the AMDs in lisa.escience.nbi.dk can only handle 256
    workgroup_size_delta = max_workgroup_size

    dists_local_mem = False

    dists_min_WS = (local_mem_size - 100) // (4 * dim_count)
    dists_min_WS = (local_mem_size - 100) // (16 * dim_count)
    dists_min_WS = min(max_workgroup_size, dists_min_WS)
    dists_min_WS = 32
    local_mean_count = 512
    #local_mean_count = dists_min_WS
    #dists_min_WS = 1
    #new_shape = points.shape
    #new_shape[0] = pad(point_count, dists_min_WS)
    #points.reshape(new_shape)

    workgroup_size_new_means = (local_mem_size - 100) // (4 * (dim_count + 1))
    workgroup_size_new_means = min(max_workgroup_size,
                                   workgroup_size_new_means)

    if 'opencl' in options and options['opencl']:
        if device_name[0:8] == 'Intel(R)' and platform_name == 'Apple':
            workgroup_size_new_means = 1

    step_size = (local_mem_size - 100) // (4 * (dim_count + 1) * workgroup_size_new_means)

    if 'opencl' in options and options['opencl']:
        if device_name[0:8] == 'Intel(R)' and platform_name == 'Apple':
            step_size = 1

    if step_size == 1:
        step_size = 2
        workgroup_size_new_means //= 2

    assert local_mem_size > 4 * (dim_count + 1) * workgroup_size_new_means * step_size
    assert step_size >= 1

    if 'verbose' in options and options['verbose']:
        print 'Step size:', step_size

    current_mean_count = mean_count

    lbg_steps = 0
    if 'LBG' in options and options['LBG'] > 0:
        print 'LBG'

        means = np.zeros((mean_count, dim_count))
        means[0] = np.average(points, 0)

        import math
        current_mean_count = 1
        lbg_steps = int(round(math.log(mean_count, 2)))


    X_buf_size = points.astype(np.float32).nbytes
    m_buf_size = means.astype(np.float32).nbytes

    template_var_dict = module_options # because of loop-unrolling and possibly other stuff? I should really fix thi
    template_var_dict.update(options) # because of loop-unrolling and possibly other stuff? I should really fix this
    template_var_dict.update({
        #'wavefront_size': wavefront_size,
        'workgroup_size_new_means': workgroup_size_new_means,
        'step_size': step_size,
        'point_count': point_count,
        'mean_count': mean_count,
        'dim_count': dim_count,

        'max_delta': default_max_delta,

        'lbg_small_number': 0.001,

        #'stop_kernel_delta': 'if(*delta < %f) { return; }' % default_max_delta,
        'stop_kernel_delta': '',

        'pass_integer_constants': False,
        'use_consecutive_banks': False,
        'dists_local_mem': dists_local_mem,
        'local_mean_count': local_mean_count,
    })


    #print 'dists local mem:', kernels['dists'].get_work_group_info(cl.kernel_work_group_info.LOCAL_MEM_SIZE, ctx.devices[0])
    #print 'dists local mem:', kernels['dists'].get_work_group_info(cl.kernel_work_group_info.PRIVATE_MEM_SIZE, ctx.devices[0])
    #print 'dists local mem:', kernels['dists'].get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, ctx.devices[0])
    #print kernels['dists'].get_work_group_info(
    #        cl.kernel_work_group_info.PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
    #        ctx.devices[0])

    workgroup_sizes = {}
    workgroup_sizes['dists_min'] = [0,0]
    workgroup_sizes['dists_min'][0] = (pad(point_count, dists_min_WS), 1)
    workgroup_sizes['dists_min'][1] = (dists_min_WS, 1)
    workgroup_sizes['new_means'] = [0,0]
    workgroup_sizes['new_means'][0] = (workgroup_size_new_means, (current_mean_count-1) / step_size + 1)
    workgroup_sizes['new_means'][1] = (workgroup_size_new_means, 1)
    workgroup_sizes['set_buffer'] = [0,0]
    workgroup_sizes['set_buffer'][0] = (1, )
    workgroup_sizes['set_buffer'][1] = (1, )
    workgroup_sizes['calc_delta'] = [0,0]
    workgroup_sizes['calc_delta'][0] = (1, workgroup_size_delta * ((current_mean_count-1) / workgroup_size_delta + 1))[::-1]  # Global size
    workgroup_sizes['calc_delta'][1] = (1, workgroup_size_delta)[::-1]  # None, #(1, wavefront_size), # Local size (workgroup size)
    workgroup_sizes['split_means'] = [0,0]
    workgroup_sizes['split_means'][0] = (1, 512 * ((mean_count-1) / 512 + 1))[::-1]  # Global size
    workgroup_sizes['split_means'][1] = (1, 512)[::-1]  # None, #(1, wavefront_size), # Local size (workgroup size)
    workgroup_sizes['sum_delta'] = [0,0]

    delta = np.zeros((mean_count,)).astype(np.float32)
    delta[:] = np.float32(np.inf)
    last_delta = delta.copy()

    start = time.time()
    buffers = {}
    buffers['X_T'] = allocate_buffer(ctx, queue, points, name='X_T')
    buffers['m_T'] = allocate_buffer(ctx, queue, means, name='m_T')
    buffers['m_new_T'] = allocate_buffer(ctx, queue, means, name='m_new_T')
    buffers['delta'] = allocate_buffer(ctx, queue, delta, name='delta')
    buffers['min_idxs'] = allocate_buffer(ctx, queue, 4 * point_count, name='min_idxs')
    workgroup_count = pad(mean_count, max_workgroup_size) / max_workgroup_size
    buffers['sum_delta_reduce'] = allocate_buffer(ctx, queue, 4 * workgroup_count, name='sum_delta_reduce')
    stop = time.time()
    print "Memory transfers: %.2f s" % (stop - start)

    # Create weight buffers
    template_var_dict['define_weighted'] = ''
    if ('weighted' in options and options['weighted']) or \
       ('log_weighted' in options and options['log_weighted']):
        template_var_dict['define_weighted'] = '#define WEIGHTED 1'

        points_weights_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=np.asfortranarray(weights.astype(np.float32)))
        m_weights_buf     = cl.Buffer(ctx, mf.WRITE_ONLY, 4 * mean_count)
        m_new_weights_buf = cl.Buffer(ctx, mf.WRITE_ONLY, 4 * mean_count)

        new_means_arglist += [points_weights_buf, m_new_weights_buf]
    # Take logarithm of weights
    if 'log_weighted' in options and options['log_weighted']:
        log_weights(ctx, queue, point_count, points_weights_buf, weights, max_workgroup_size, template_var_dict)

    compile_time = {}
    kernels = {}
    kernels['dists_min'] = compile_kernel(ctx, kernels_module.dists_min_code, 'dists_min', template_var_dict, compile_time)
    kernels['new_means'] = compile_kernel(ctx, kernels_module.new_means_code, 'new_means_dim', template_var_dict, compile_time)
    kernels['set_buffer'] = compile_kernel(ctx, kernels_module.set_buffer_code, 'set_buffer', template_var_dict, compile_time)
    kernels['calc_delta'] = compile_kernel(ctx, kernels_module.calc_delta_code, 'calc_delta', template_var_dict, compile_time)
    kernels['split_means'] = compile_kernel(ctx, kernels_module.split_means_code, 'split_means', template_var_dict, compile_time)
    sum_delta_template = {
        'reduction_op': 'A + B',
        'neutral_element': '0.0f',
        'max_workgroup_size': max_workgroup_size,
    }
    kernels['sum_delta'] = compile_kernel(ctx, kernels_module.reduce_code, 'reduce', dict(template_var_dict, **sum_delta_template))
    print 'Compile time: %.2f s' % sum(compile_time.values())

    arg_lists = {}
    arg_lists['dists_min'] = [buffers['X_T'], buffers['min_idxs']]
    arg_lists['new_means'] = [buffers['X_T'], buffers['min_idxs']]
    arg_lists['set_buffer'] = [buffers['delta']]
    arg_lists['calc_delta'] = [buffers['m_T'], buffers['m_new_T'], buffers['delta']]
    arg_lists['split_means'] = [buffers['m_T']]

    max_step_count = default_max_step_count
    if 'max_step_count' in options:
        max_step_count = options['max_step_count']

    if 'log_steps' in options and options['log_steps']:
        m_log = np.zeros((max_step_count, mean_count, dim_count), dtype=np.float32)
        print 'Max step count: %u' % max_step_count

    dim_ranges = np.max(points, 0) - np.min(points, 0)
    diagonal = np.sqrt(np.sum(dim_ranges ** 2))

    start = time.time()
    for i_lbg in xrange(lbg_steps + 1):
        print 'LBG step, means: %u' % current_mean_count

        workgroup_sizes['new_means'][0] = (workgroup_size_new_means, (current_mean_count-1) / step_size + 1)
        workgroup_sizes['new_means'][1] = (workgroup_size_new_means, 1)

        workgroup_sizes['calc_delta'][0] = (1, workgroup_size_delta * ((current_mean_count-1) / workgroup_size_delta + 1))[::-1]  # Global size
        workgroup_sizes['calc_delta'][1] = (1, workgroup_size_delta)[::-1]  # None, #(1, wavefront_size), # Local size (workgroup size)

        # Set delta to +infinity
        set_buffer_event = call_kernel('set_buffer', queue, kernels, workgroup_sizes, arg_lists, [np.float32(np.inf)], options)
        set_buffer_event.wait()

        step_count = 0
        for i in xrange(max_step_count):
            step_count = i

            # Execute kernels
            dists_min_event = call_kernel('dists_min', queue, kernels, workgroup_sizes, arg_lists, [buffers['m_T'], np.uint32(current_mean_count)], options)
            new_means_event = call_kernel('new_means', queue, kernels, workgroup_sizes, arg_lists, [buffers['m_new_T'], np.uint32(current_mean_count)], options)
            set_buffer_event = call_kernel('set_buffer', queue, kernels, workgroup_sizes, arg_lists, [np.float32(0)], options)
            calc_delta_event = call_kernel('calc_delta', queue, kernels, workgroup_sizes, arg_lists, [np.uint32(current_mean_count)], options)

            value_count = current_mean_count
            values_buf = buffers['delta']
            while True:
                workgroup_sizes['sum_delta'][0] = (pad(value_count, max_workgroup_size), 1)
                workgroup_sizes['sum_delta'][1] = (max_workgroup_size, 1)
                arg_lists['sum_delta'] = [values_buf, buffers['sum_delta_reduce'], np.uint32(value_count)]
                sum_delta_event = call_kernel('sum_delta', queue, kernels, workgroup_sizes, arg_lists, [], options)
                workgroup_count = pad(value_count, max_workgroup_size) / max_workgroup_size
                value_count = workgroup_count
                values_buf = buffers['sum_delta_reduce']
                sum_delta_event.wait()
                if value_count == 1: break

            sum_delta = np.zeros((1,)).astype(np.float32)
            sum_delta_mem_event = transfer_memory(queue, sum_delta, buffers['sum_delta_reduce'])
            sum_delta_mem_event.wait()

            # Wait for last event in step - to ensure that this step is done
            # before we enqueue a new step
            if 'opencl' in options and options['opencl']:
                calc_delta_event.wait()
            elif 'cuda' in options and options['cuda']:
                pycuda.driver.Context.synchronize()

            #print clarray.Array(queue, (dim_count, mean_count), np.float32, order="F", data=m_T_buf) - \
            #      clarray.Array(queue, (dim_count, mean_count), np.float32, order="F", data=buffers['m_new_T'])

            if 'log_steps' in options and options['log_steps']:
                m = np.zeros((dim_count, mean_count)[::-1], order='F', dtype=np.float32)
                log_step_mem_event = transfer_memory(queue, m, buffers['m_T'])
                event_history.log('log_step_mem_event',  log_step_mem_event)
                log_step_mem_event.wait()
                m_log[step_count] = m
                print m_log[step_count]

            # Swap new means and old means
            temp = buffers['m_T']
            buffers['m_T'] = buffers['m_new_T']
            buffers['m_new_T'] = temp

            if 'check_delta' in options and options['check_delta']:
                delta_mem_copy_event = transfer_memory(queue, delta, buffers['delta'])
                delta_mem_copy_event.wait()
                event_history.log('delta_mem_copy_event', delta_mem_copy_event)

                if 'verbose' in options and options['verbose'] >= 2:
                    print delta
                    print sum(delta)
                    print sum_delta

                # If relative average delta is less than 0.1%
                if sum_delta[0] / mean_count / diagonal < 0.001:
                    break

                if np.allclose(delta, last_delta):
                    break
                delta, last_delta = last_delta, delta

                #if delta[0] < default_max_delta:
                #    print 'Break'
                #    break


            #temp = buffers['delta']
            #buffers['delta'] = buffers['delta']_new
            #buffers['delta']_new = temp

        print 'Done in %s/%s steps' % (step_count+1, max_step_count)

        if current_mean_count < mean_count:
            print 'Splitting'
            # Add means
            workgroup_sizes['split_means'][0] = (1, 512 * ((current_mean_count-1) / 512 + 1))[::-1]
            split_means_event = call_kernel('split_means', queue, kernels, workgroup_sizes, arg_lists, [np.uint32(current_mean_count)], options)
            split_means_event.wait()
            current_mean_count *= 2
            current_mean_count = min(mean_count, current_mean_count)

    stop = time.time()
    print "Computation: %.2f s" % (stop - start)

    # Transfer data back (get results)
    print 'Creating ndarrays...',
    m        = np.zeros((dim_count, mean_count)[::-1], order='F', dtype=np.float32)
    if 'check_result' in options and options['check_result']:
        min_idxs = np.zeros((point_count, 1)).astype(np.uint32)
        m_new    = np.zeros((dim_count, mean_count)[::-1], order='F', dtype=np.float32)
        delta     = np.zeros((1,)).astype(np.float32)
    print 'Done'
    print 'Transferring data...',
    transfer_memory(queue, m       , buffers['m_T']     ).wait()
    if 'check_result' in options and options['check_result']:
        transfer_memory(queue, min_idxs, buffers['min_idxs']).wait()
        transfer_memory(queue, m_new   , buffers['m_new_T'] ).wait()
        transfer_memory(queue, delta    , buffers['delta']    ).wait()
    print 'Done'

    # We switch between two means buffers (buffers['m_T'] and buffers['m_new_T'])
    # every iteration, and so we should take the correct one (if we don't stop
    # at the correct one) -- when using max_steps to stop and not check_delta
    #delta_new = np.zeros((1,)).astype(np.float32)
    #cl.enqueue_copy(queue, delta_new, buffers['delta']_new).wait()
    #if delta_new < options['max_delta']:
        #print 'Switch'
        #m = m_new

    #print m
    #print m_new

    if 'check_result' in options and options['check_result']:
        return dists[0:point_count, :], min_idxs[0:point_count, 0], mins[0:point_count, 0], m_new[0:mean_count]
    else:
        return m[0:mean_count]


def benchmark(options):
    import time
    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # Generate test sets
    workgroup_size = options['workgroup_size']
    point_count = options['point_count']
    mean_count = options['mean_count']
    dim_count = options['dim_count']
    sample_count = options['sample_count']

    print 'Sample count:', sample_count

    points = np.random.rand(sample_count, point_count, dim_count)
    means = np.random.rand(sample_count, mean_count, dim_count)

    # Parameter passing vs. integer constants
    param_settings = {
        'mean_count': mean_count,
        'dim_count': dim_count,
        'point_count': point_count,
        'workgroup_size': workgroup_size,
        'workgroup_size0': workgroup_size,
        'workgroup_size1': workgroup_size,
        #'dims': range(dim_count),
        #'dimLoopUnroll': False,
        'pass_integer_constants': False,
    }

    #print str(Template(kernels.dists_code).render(param_settings))
    param_dists_kernel = cl.Program(ctx, kernels_module.dists_code, 'param_dists', param_settings, options)

    const_times = np.zeros(sample_count)
    compile_times = np.zeros(sample_count)
    param_times = np.zeros(sample_count)
    local_times = np.zeros(sample_count)
    local_dim_times = np.zeros(sample_count)

    for i in xrange(sample_count):
        point_count, dim_count = points[i].shape
        mean_count, dim_count2 = means[i].shape

        # Create and transfer memory
        mf = cl.mem_flags
        X_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=points[i].astype(np.float32))
        m_buf = cl.Buffer(ctx, mf.COPY_HOST_PTR, hostbuf=means[i].astype(np.float32))
        X_T_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=np.asfortranarray(points[i].astype(np.float32)))
        buffers['m_T'] = cl.Buffer(ctx, mf.COPY_HOST_PTR, hostbuf=np.asfortranarray(means[i].astype(np.float32)))
        buffers['dists'] = cl.Buffer(ctx, mf.WRITE_ONLY, 4 * point_count * mean_count)

        # Build kernel
        const_settings = {
            'mean_count': mean_count,
            'dim_count': dim_count,
            'point_count': point_count,
            'workgroup_size': workgroup_size,
            'dims': range(dim_count),
            'pass_integer_constants': False,
        }

        compile_start = time.time()
        const_dists_kernel = compile_kernel(ctx, kernels_module.dists_code, 'const_dists', const_settings)
        compile_end = time.time()
        compile_times[i] = compile_end - compile_start

        # Call kernels (these cannot be run in parallel (it causes crashes)
        #param_event = param_dists_kernel.dists(queue,
                                               #(point_count, ), # Global size
                                               #None, # Local size (workgroup size)
                                                ##Kernel arguments
                                               #X_buf, m_buf, buffers['dists'],
                                               ##np.uint32(mean_count), np.uint32(dim_count)
                                              #)
        #param_event.wait()

        #const_event = const_dists_kernel.dists(queue, (point_count, ), None, X_buf, m_buf, buffers['dists'])
        #const_event.wait()

        #local_event = const_dists_kernel.dists_local(queue,
                #((point_count-1) / workgroup_size + 1, workgroup_size * ((mean_count-1) / workgroup_size + 1)),
                #(1, workgroup_size), # None
                #X_buf, m_buf, buffers['dists'],
                #cl.LocalMemory(4 * dim_count * workgroup_size),
                #)
        #local_event.wait()

        local_dim_event = const_dists_kernel.dists_local_dim(
            queue,
            ((point_count-1) / workgroup_size + 1, workgroup_size * ((mean_count-1) / workgroup_size + 1)),
            (1, workgroup_size),  # None
            buffers['X_T'], buffers['m_T'], buffers['dists'],
            cl.LocalMemory(4 * dim_count * workgroup_size),
        )
        local_dim_event.wait()

        #const_times[i] = (const_event.profile.end - const_event.profile.start) / 10.0**9
        #param_times[i] = (param_event.profile.end - param_event.profile.start) / 10.0**9
        #local_times[i] = (local_event.profile.end - local_event.profile.start) / 10.0**9
        local_dim_times[i] = (local_dim_event.profile.end - local_dim_event.profile.start) / 10.0**9

    print "Compile time %0.3f ± %0.3f s" % (np.mean(compile_times), np.std(compile_times))
    print "Constant time %0.3f ± %0.3f s" % (np.mean(const_times), np.std(const_times))
    print "Parameter time %0.3f ± %0.3f s" % (np.mean(param_times), np.std(param_times))
    print "Local memory: %0.3f ± %0.3f s" % (np.mean(local_times), np.std(local_times))
    print "Local and dimension sorting memory: %0.3f ± %0.3f s" % (np.mean(local_dim_times), np.std(local_dim_times))

    if options['check_result']:
        # Transfer data back (get results)
        dists = np.zeros((point_count, mean_count)).astype(np.float32)
        cl.enqueue_copy(queue, dists, buffers['dists']).wait()

        return points[-1], means[-1], dists[0:point_count, :]


if __name__ == '__main__':
    import cliargs
    args, options = cliargs.get_options()

    dim_count = args.dim_count
    point_count = args.point_count
    mean_count = args.mean_count
    workgroup_size = args.workgroup_size  # 1920

    if not args.fortran:
        print 'Point count:', point_count
        print 'Mean count:', mean_count
        print 'Dimension count:', dim_count
    print 'Workgroup size:', workgroup_size

    max_step_count = 1
    # If max_steps has been set
    if args.max_steps > 0:
        max_step_count = args.max_steps
    # If max_steps has not been set and we're in LBG-mode
    elif args.LBG:
        max_step_count = default_max_step_count  # don't choose max_step = 1

    template_vars = {
        'workgroup_size': workgroup_size,

        'format_string': '%f',
        'printf': False,

        'max_delta': default_max_delta,

        'lbg_small_number': 0.001,
    }


    #options = {
        #'dim_count': dim_count,
        #'point_count': point_count,
        #'mean_count': mean_count,
        #'workgroup_size': workgroup_size,
        #'sample_count': args.samples,
        #'profiling': args.profiling,
        #'check_result': args.check_result,
        #'verbose': args.verbose,
        #'max_step_count': max_step_count,
        #'max_delta': args.max_delta,
        #'lbg': args.LBG,
        #'check_delta': args.check_delta,
        #'template_vars': template_vars
    #}
    options.update(template_vars)

    #print 'Maximum total workgroup size:', ctx.devices[0].max_work_group_size
    #print 'Maximum workgroup sizes:', ctx.devices[0].max_work_item_sizes

    points, means = None, None
    dists, min_idxs, mins, gpu_m = None, None, None, None
    if args.benchmark:
        points, means, dists = benchmark(options)
    elif args.fortran:
        #points = np.loadtxt('fortran/6d_data.txt')
        #points = np.loadtxt('fortran/20000.txt')
        points = np.loadtxt('fortran/100000.txt')
        points = points[:, 0:6]  # We want the first six columns
        point_count, dim_count = points.shape

        #mean_count = 128

        print 'Point count:', point_count
        print 'Mean count:', mean_count
        print 'Dimension count:', dim_count

        import time
        means = np.random.rand(mean_count, dim_count)
        means = np.zeros((mean_count, dim_count))
        start = time.time()
        kmeans(points, means, options)
        stop = time.time()

        print 'Full time: %s s' % (stop - start)

    else:
        #points = np.array([[1,2],[3,4]])
        #means = np.array([[0,0],[1,1]])

        points = np.random.rand(point_count, dim_count)
        means = np.random.rand(mean_count, dim_count)

        if ('weighted' in options and options['weighted']) or ('log_weighted' in options and options['log_weighted']):
            weights = np.random.rand(point_count, 1)
        else:
            weights = None

        if args.check_result:
            dists, min_idxs, mins, gpu_m = kmeans(points, means, options, weights)
        else:
            gpu_m = kmeans(points, means, options, weights)
        print 'GPU calculation done.'

        X = np.matrix(points)
        m = np.matrix(means)
        #print m
        #print 'GPU distances', dists
        #print 'Numpy distances', np_dists
        if args.check_result and not args.no_numpy:

            np_dists = np.sum(np.power(X, 2), 1) + \
                       np.sum(np.power(m, 2), 1).T - \
                       2 * X * m.T

            np_mins = np.min(np_dists, 1)
            np_min_idxs = np.argmin(np_dists, 1)
            mins_indices = np.indices(np_mins.shape)[0]
            np_m = m.copy()

            for i in xrange(mean_count):
                if sum(np_min_idxs == i) > 0:
                    # Set the new mean to the centroid of the cluster
                    # cluster = set of points assigned to this mean
                    np_m[i] = np.average(X[mins_indices[np_min_idxs == i]], 0)

            if args.verbose:
                print 'Points'
                print X
                print 'GPU / Numpy distances'
                print np.column_stack((dists, np_dists))
                if not mins is None:
                    print 'GPU / Numpy minima'
                    print np.array(np_mins).ravel().shape, mins.shape
                    print np.column_stack((mins, np_mins))
                if not min_idxs is None:
                    print 'GPU / Numpy minima indixes'
                    print np.array(np_min_idxs).ravel().shape, min_idxs.shape
                    print np.column_stack((min_idxs, np_min_idxs))
                if not gpu_m is None:
                    print 'GPU / Numpy new means'
                    print np.column_stack([gpu_m, np_m])

            try:
                if not np.allclose(dists, np_dists):
                    import warnings
                    warnings.warn("GPU and Numpy distances not equal")

                if not np.allclose(mins, np.array(np_mins).ravel()):
                    import warnings
                    warnings.warn("GPU and Numpy minima not equal")

                if not np.array_equal(min_idxs, np.array(np_min_idxs).ravel()):
                    import warnings
                    warnings.warn("GPU and Numpy minima indices not equal")

                if not np.allclose(gpu_m, np_m):
                    import warnings
                    warnings.warn("GPU and Numpy means not equal")
            except:
                pass

    if args.event_history:
        event_history.save('event_history.pickle')
        event_history.print_timings()
        event_history.graph()



# vim: let g:pymode_lint = 0
# vim: filetype=pyopencl.python
