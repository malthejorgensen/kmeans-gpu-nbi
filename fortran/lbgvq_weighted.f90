! $Id: lbgvq_weighted.f90,v 1.10 2012/12/13 11:32:27 trier Exp $

MODULE LBGVQ
IMPLICIT NONE
 INTEGER, PARAMETER :: my_real_kind = 4
 INTEGER, PARAMETER :: my_int_kind  = 4
END MODULE LBGVQ

!-------------------------------------------------------------------------------!
 SUBROUTINE lbgvq_weighted( dims,                       &                       ! dimensionality
                            no_tr_vec, trainv, weights, &                       ! number of training vecs, vectors, weights
                            cb_size,   codebk, codew     )                      ! code book size, vectors, weights

 USE lbgvq, only : my_int_kind, my_real_kind
!f2py intent(in) :: dims, no_tr_vec, trainv, weights, cb_size
!f2py intent(out) :: codebk, codew

 IMPLICIT NONE

   integer(kind=my_int_kind) :: dims                                            ! number of dimensions (usually about 6...)
   integer(kind=my_int_kind) :: no_tr_vec                                       ! number of training vectors
   integer(kind=my_int_kind) :: cb_size                                         ! number of codebook vectors
   integer(kind=my_int_kind) :: mtemp, old_size                                 ! present number of codebooks in this iteration

   integer(kind=my_int_kind) :: i, j, k                                         ! loop counters
   integer(kind=my_int_kind) :: index, sign, flag                               ! index, sign of split perturbation, split pertubation comp.wise, flag for done / not done

   real(kind=my_real_kind)   :: w1                                              ! weight of 1 training vec
   real(kind=my_real_kind)   :: totw                                            ! cumul. weight, training vecs
   real(kind=my_real_kind)   :: del, rnd, seed, compon                          ! a random number, and some stuff
   real(kind=my_real_kind)   :: iw, wi, wj, drel, dist, dist1, dist2            ! a vector weight, do., do. for matching, relative distortion, distance btw vecs, 2 x comparison distance vars
   real(kind=my_real_kind)   :: totd1, totd2                                    ! global distortion measure 1 and 2
   real(kind=my_real_kind)   :: t1, t2, dt                                      ! timing vars

   integer(kind=my_int_kind), dimension(cb_size)        :: bin                  ! bin array for count of trainingvectors-per-codebookvector association

   real(kind=my_real_kind),   dimension(no_tr_vec,dims) :: trainv               ! training vectors (indata)
   real(kind=my_real_kind),   dimension(cb_size,dims)   :: codebk               ! codebook vectors (outdata)
   real(kind=my_real_kind),   dimension(cb_size,dims)   :: yy                   ! auxill. codebook
   real(kind=my_real_kind),   dimension(no_tr_vec)      :: weights              ! weigths of training vectors
   real(kind=my_real_kind),   dimension(cb_size)        :: codew                ! weigths of code book vectors


   if (no_tr_vec .le. cb_size) then                                   ! we should have fewer code book vecs than training vecs...
    write(*,*) 'too many codebook vecs -- or too few training vecs' 
    stop
   endif

   call cpu_time(t1)                                                  ! time it...

   mtemp = 1                                                                       ! present number of code book vecs (initially == 1)

   do k=1,dims
    codebk(1,k) = 0.0                                                 ! reset first code book vector (componentwise)
    totw = 0.0                                                        ! total cumul. weight
    do i=1,no_tr_vec
     iw=weights(i)
     totw=totw+iw
     codebk(1,k) = codebk(1,k) + trainv(i,k)*iw                                    ! sum all training vectors into first code book vector (componentwise)  
    enddo
    codebk(1,k) = codebk(1,k)/(totw)                                  ! average to find first centroid (componentwise), continuous weights
   enddo
   codew(1) = totw                                                    ! assign the total weight associated with this codebook vector

!------------------------- MAIN LOOP BEGIN ---------------------------!
 do
  if (mtemp .lt. cb_size) then                                        ! make sure that we don't overshoot
   old_size = mtemp
   mtemp=mtemp*2                                                      ! double the number of code book vecs
   if (mtemp .gt. cb_size) then
     mtemp = cb_size
   endif
  !----------------DISPLACE NEW BATCH OF CODE BOOK VECS---------------! CB vector splitting
   do i=1,mtemp                                                       ! loop over new CB batch
    do k=1,dims
     if (i .lt. old_size) then                                         ! Change by Malthe (also commented the three lines above)
      yy(i,k) = codebk(i,k)
     else
      yy(i,k) = codebk(i-old_size,k)+0.001                                    ! now do splitting into new codebook vectors (componentwise)
     endif
     !call random_number(seed)
     !sign=merge(-1.0,1.0,seed.le.0.5)                                 ! sign of displacement is random...
     !call random_number(seed)
     !compon=seed                                                      ! component shift is random...
     !del=0.001*compon*sign                                            ! relative shift of newly formed (split) code book vecs...
     !if (i .eq. (i/2)*2) del=-del                                     ! ... the other half of the new CB vecs.
     !yy(i,k) = codebk(j,k)*(1.+del)                                   ! now do splitting into new codebook vectors (componentwise)
    enddo
   enddo
  !-------------------------------------------------------------------!
   totd2=1.0e15                                                       ! some great distance to compare against initially (hopefully big enough!)
   flag=0                                                             ! flag, 0 == not done yet.
  !------------- DO ACTUAL ASSIGNMENT OF TRAINING VECS ---------------!
   do
    if (flag .eq. 0) then
     totd1=0.0                                                        ! zero total distortion measure 1
     !---------------------FORGET LAST CODEBOOK-----------------------!
     do i=1,mtemp                                                     ! loop over all CB vecs ...
       do k=1,dims                                                    ! ... and dimensions
         codebk(i,k)=0.0                                              ! zero all CB vecs
       enddo
       bin(i)=0                                                       ! zero all 'bin' (number of trainv in the codebk vecs) values
       codew(i)=0.0                                                   ! zero all corresp. codebk weight values
     enddo
     !--------------------LOOP OVER TRAINING VECS---------------------!
     do i=1,no_tr_vec
       j=1                                                            ! init 'j'
       dist2=0.0                                                      ! init distortion
       wi=weights(i)                                                  ! weight of this i'th training vector
       do k=1,dims
         ! Malthe: why weight the distance measurement?
         ! the weight is on the training vector (the data point) so
         ! it makes no diffence as to which mean will be closest
         ! and so it should make no difference to the final result
         dist2=dist2+wi*((trainv(i,k)-yy(j,k))**2)                    ! distortion measure 2 w/first CB vector, componentwise, weighted
       enddo
       index=1                                                        ! init index <= test first codebook vector (number '0') first 
       !----------- LOOP OVER CB VECS (except the first) -------------!
       do j=2,mtemp
         dist1=0.0                                                    ! init distortion measure 1
         do k=1,dims
           dist1=dist1+wi*((trainv(i,k)-yy(j,k))**2)                  ! distortion measure 1 sum over dimensions, componentwise, weigthed
         enddo
         if (dist1 .lt. dist2) then                                   ! choose smallest distortion, and accomp. index
           dist2=dist1 
           index=j                                                    ! update index for this codebk vec 
         endif                                                        ! 
       enddo
       !--------------------------------------------------------------!
       bin(index)=bin(index)+1                                        ! increment 'bin' at right index
       wj=wi                                                          ! associate weight with j'th code book vector
       codew(index) = codew(index)+wj                                 ! increment total weight on cluster at right index
       do k=1,dims
         codebk(index,k)=codebk(index,k)+trainv(i,k)*wi               ! switch to more correct codebk for this training vector, weighted
       enddo
       totd1=totd1+dist2                                              ! add dist2 to total distortion 1  
     enddo
     !------------------- LOOP OVER ALL CB VECS ----------------------!
     do j=1,mtemp
       if (codew(j) .gt. 0.0) then                                    ! only do if j'th cluster not empty
         do k=1,dims
           codebk(j,k)=codebk(j,k)/codew(j)                           ! correctly normalize codebook vectors, weighted
           yy(j,k)=codebk(j,k)                                        ! make new generation codebk copy
         enddo
       endif
     enddo
     !----------------------------------------------------------------!
     totd1=totd1/real(no_tr_vec*dims)                                 ! renormalize total distortion measure 1
     drel=(totd2-totd1)/totd1                                         ! relative distortion
     flag=1                                                           ! default is now we're done...
     if (drel .gt. 0.001) then                                        ! unless too much distortion still.
      flag=0                                                          ! re-unflag
      totd2=totd1                                                     ! new distortion measure 2
     endif
     !----------------------------------------------------------------!
     else
      exit
     endif
    enddo
   !------------------------------------------------------------------!
  else
   exit
  endif
 enddo
!-------------------------- MAIN LOOP END ----------------------------!

 call cpu_time(t2)                                                    ! ...done timing it.
 write(*,*) 'Average distortion        = ',totd1                      ! absolute distortion, should somehow be adjusted to the cooridnate ranges to reflect a relative error instead.....but how?
 write(*,*) 'Total time spent in LBGVQ = ',t2-t1                      ! timing
END SUBROUTINE lbgvq_weighted
!----------------------------------------------------------------------------------!
