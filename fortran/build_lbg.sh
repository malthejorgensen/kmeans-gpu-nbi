gfortran -O3 test_6d_fortran.f90 lbgvq_weighted.f90 -o test_6d_fortran
#gfortran -O3 test_6d_fortran.f90 lbgvq_weighted_log.f90 -o test_6d_fortran


# Build Python module
# https://github.com/thehackerwithin/PyTrieste/wiki/F2Py
#f2py -c -m lbgvq_weighted lbgvq_weighted.f90
f2py -DF2PY_REPORT_ON_ARRAY_COPY=1 -c -m lbgvq_weighted lbgvq_weighted.f90
f2py -DF2PY_REPORT_ON_ARRAY_COPY=1 -c -m lbgvq_weighted_log lbgvq_weighted_log.f90
