! $Id: kmeans_weighted.f90,v 1.10 2012/12/13 11:32:27 trier Exp $

MODULE kmeans
IMPLICIT NONE
 INTEGER, PARAMETER :: my_real_kind = 4
 INTEGER, PARAMETER :: my_int_kind  = 4
END MODULE kmeans

!-------------------------------------------------------------------------------!
 SUBROUTINE kmeans_weighted( dims, max_delta,           &                       ! dimensionality
                            no_tr_vec, trainv, weights, &                       ! number of training vecs, vectors, weights
                            cb_size,   codebk, codew     )                      ! code book size, vectors, weights

 USE kmeans, only : my_int_kind, my_real_kind
!f2py intent(in) :: dims, no_tr_vec, trainv, weights, cb_size, codebk, max_delta
!f2py intent(out) :: codebk, codew

 IMPLICIT NONE

   integer(kind=my_int_kind) :: dims                                            ! number of dimensions (usually about 6...)
   integer(kind=my_int_kind) :: no_tr_vec                                       ! number of training vectors
   integer(kind=my_int_kind) :: cb_size                                         ! number of codebook vectors
   integer(kind=my_int_kind) :: mtemp                                           ! present number of codebooks in this iteration

   integer(kind=my_int_kind) :: i, j, k                                         ! loop counters
   integer(kind=my_int_kind) :: index, sign, flag                               ! index, sign of split perturbation, split pertubation comp.wise, flag for done / not done

   real(kind=my_real_kind)   :: max_delta, current_min_dist, delta_sum
   real(kind=my_real_kind)   :: w1                                              ! weight of 1 training vec
   real(kind=my_real_kind)   :: totw                                            ! cumul. weight, training vecs
   real(kind=my_real_kind)   :: del, rnd, seed, compon                          ! a random number, and some stuff
   real(kind=my_real_kind)   :: iw, wi, wj, drel, dist, dist1, dist2            ! a vector weight, do., do. for matching, relative distortion, distance btw vecs, 2 x comparison distance vars
   real(kind=my_real_kind)   :: t1, t2, dt                                      ! timing vars

   integer(kind=my_int_kind), dimension(cb_size)        :: bin                  ! bin array for count of trainingvectors-per-codebookvector association

   real(kind=my_real_kind),   dimension(no_tr_vec,dims) :: trainv               ! training vectors (indata)
   real(kind=my_real_kind),   dimension(cb_size,dims)   :: codebk               ! codebook vectors (outdata)
   real(kind=my_real_kind),   dimension(cb_size,dims)   :: last_codebk                   ! auxill. codebook
   real(kind=my_real_kind),   dimension(no_tr_vec)      :: weights              ! weigths of training vectors
   real(kind=my_real_kind),   dimension(cb_size)        :: codew                ! weigths of code book vectors
   real(kind=my_real_kind),   dimension(cb_size,dims)   :: delta                ! weigths of code book vectors


   if (no_tr_vec .le. cb_size) then                                   ! we should have fewer code book vecs than training vecs...
    write(*,*) 'too many codebook vecs -- or too few training vecs' 
    stop
   endif

   call cpu_time(t1)                                                  ! time it...

   mtemp = cb_size                                                    ! present number of code book vecs (initially == 1)

!------------------------- MAIN LOOP BEGIN ---------------------------!
  !----------------DISPLACE NEW BATCH OF CODE BOOK VECS---------------! CB vector splitting
  !-------------------------------------------------------------------!
   flag=0                                                             ! flag, 0 == not done yet.
  !------------- DO ACTUAL ASSIGNMENT OF TRAINING VECS ---------------!
    do
     write(*,*) 'Fortran step'
     if (flag .eq. dims*cb_size) then
      exit
     endif
     !---------------------FORGET LAST CODEBOOK-----------------------!
     do i=1,mtemp                                                     ! loop over all CB vecs ...
      do k=1,dims                                                    ! ... and dimensions
        last_codebk(i,k) = codebk(i,k)
        codebk(i,k)=0.0                                              ! zero all CB vecs
      enddo
      bin(i)=0                                                       ! zero all 'bin' (number of trainv in the codebk vecs) values
      codew(i)=0.0                                                   ! zero all corresp. codebk weight values
     enddo
     !--------------------LOOP OVER TRAINING VECS---------------------!
     do i=1,no_tr_vec
      wi=weights(i)                                                  ! weight of this i'th training vector
      index=0                                                        ! init index <= test first codebook vector (number '0') first 
      current_min_dist = 1e30
      !----------- LOOP OVER CB VECS (except the first) -------------!
      do j=1,mtemp
       dist1=0.0                                                    ! init distortion measure 1
       do k=1,dims
        ! Malthe: why weight the distance measurement?
        ! the weight is on the training vector (the data point) so
        ! it makes no diffence as to which mean will be closest
        ! and so it should make no difference to the final result
        dist1=dist1+wi*((trainv(i,k)-last_codebk(j,k))**2)                  ! distortion measure 1 sum over dimensions, componentwise, weigthed
       enddo
       if (dist1 .lt. current_min_dist) then                                   ! choose smallest distortion, and accomp. index
         current_min_dist=dist1
         index=j                                                    ! update index for this codebk vec 
       endif                                                        ! 
      enddo
      !--------------------------------------------------------------!
      bin(index)=bin(index)+1                                        ! increment 'bin' at right index
      codew(index) = codew(index)+wi                                 ! increment total weight on cluster at right index
      do k=1,dims
       codebk(index,k)=codebk(index,k)+trainv(i,k)*wi               ! switch to more correct codebk for this training vector, weighted
       !delta = trainv(i,k) - codebk(index,k);
       !codebk(index,k) = codebk(index,k) + delta*wi / codew(index);
      enddo
     enddo
     !------------------- LOOP OVER ALL CB VECS ----------------------!
     flag=0                                                           ! default is now we're done...
     delta_sum=0.0
     do j=1,mtemp
      if (codew(j) .gt. 0.0) then                                    ! only do if j'th cluster not empty
       do k=1,dims
        codebk(j,k)=codebk(j,k)/codew(j)                           ! correctly normalize codebook vectors, weighted
        if (last_codebk(j,k) - codebk(j,k) .eq. delta(j,k)) then
         flag=flag+1
        endif
        delta(j,k) = abs(last_codebk(j,k) - codebk(j,k))
        delta_sum = delta_sum + delta(j,k)
       enddo
      else
       do k=1,dims
        codebk(j,k)=last_codebk(j,k)
       enddo
      endif
     enddo
     if ((delta_sum / real(cb_size) / max_delta) .lt. 0.001) then
      write(*,*) 'delta_sum ', delta_sum
      write(*,*) 'delta_sum / cb_size ', (delta_sum / real(cb_size))
      write(*,*) 'max_delta ', max_delta
      exit
     endif
    enddo
   !------------------------------------------------------------------!
!-------------------------- MAIN LOOP END ----------------------------!

 call cpu_time(t2)                                                    ! ...done timing it.
 write(*,*) 'kmeans calculation time = ',t2-t1                      ! timing
END SUBROUTINE kmeans_weighted
!----------------------------------------------------------------------------------!
