# encoding=UTF-8
import numpy as np
import time

nks = [
    (100, 10),
    (1000, 100),
    (10000, 1000),
    (20000, 2000),
    # (30000, 3000),
]

print "(n, k)"
d = 6

import argparse
parse = argparse.ArgumentParser()
parse.add_argument('gibis', type=int, nargs='?', help='Number of gibibytes to allocate')
parse.add_argument('-n','--runs', type=int, default=1, help='Number of iterations to do')
args = parse.parse_args()
print vars(args)
r = (1,16)
if args.gibis is not None:
    r = (args.gibis, args.gibis+1)


# for n, k in nks:
for g in range(*r):
    s = g * 1024 ** 3
    k = np.sqrt(s/40)
    n = 10*k

    print "(%u, %u) running..." % (n, k)

    nk_time = []
    for i in xrange(args.runs):
        X = np.ones((n,d), dtype=np.float32)
        M = np.ones((k,d), dtype=np.float32)

        start = time.time()
        D = -2 * X.dot(M.T)
        D += np.reshape(np.sum(X**2, 1), (n, 1))
        D += np.reshape(np.sum(M**2, 1), (1, k))
        stop = time.time()
        nk_time.append(stop - start)
    avg = np.mean(nk_time)
    std = np.std(nk_time)
    print "(%u, %u) took %.4f ± %.4f s" % (n, k, avg, std)
