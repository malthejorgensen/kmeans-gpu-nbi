CL_INVALID_WORK_GROUP_SIZE
--------------------------
The error "clEnqueueNDRangeKernel: CL_INVALID_WORK_GROUP_SIZE" can at times be a
synonym for "out of resources"; this can happen at unexpected times when you
inside your kernel allocate private memory -- this uses local memory, and so
should be considered when calculating the local memory usage.

So it's basically telling you (sometimes): I'm out of local memory because your
                                           work units allocated a lot of private
                                           memory.

WORK_GROUP_SIZE * private_memory_per_work_unit + local memory allocated <= local memory

Local memory on the CPU
-----------------------
You cannot use local memory on the CPU - because it does not exist. If you do
use local memory, the workgroup size will have to be 1.

Running out of memory on AMD
----------------------------
The error "pyopencl.LogicError: create_buffer failed: invalid buffer size"
can be caused by trying to allocate a buffer that is too large to fit in global
memory. OpenCL also states that the maximal buffer size must be at least 1/4 of
global memory meaning that you can't necessarily make buffers that use the full
global memory. - But there's a fix:
Setting the following environment variables
> export GPU_MAX_HEAP_SIZE=100
> export GPU_MAX_ALLOC_PERCENT=100

References

 - <http://devgurus.amd.com/message/1230969#1230969>
 - <http://devgurus.amd.com/thread/158784>


Careful (CUDA: out of resources)
--------------------------------
I got this error

    pycuda._driver.LaunchError: cuLaunchKernel failed: launch out of resources

Because I wrote

    split_means_event = call_kernel('split_means', queue, kernels, workgroup_sizes, arg_lists, [np.uint(current_mean_count)], options)

instead of

    split_means_event = call_kernel('split_means', queue, kernels, workgroup_sizes, arg_lists, [np.uint32(current_mean_count)], options)

`uint` instead of `uint32` - obvious mistake - but not from reading the error
