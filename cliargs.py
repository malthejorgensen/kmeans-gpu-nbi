# coding=utf8
import numpy as np
import argparse
import re
import pyopencl as cl
import os

def devices(s):
    try:
        device_ids = map(int, s.split(','))
        return device_ids
    except:
        raise argparse.ArgumentTypeError("Devices must be a comma-separated list of integers")

class VAction(argparse.Action):
    def __call__(self, parser, args, values, option_string=None):
        # print 'values: {v!r}'.format(v=values)
        if values is None:
            values = '1'
        try:
            values = int(values)
        except ValueError:
            values = values.count('v') + 1
        setattr(args, self.dest, values)

def parse_means(means_str, point_count):
    mean_ratio = 0.3
    p_re = re.match(r'^(\d{1,3})%$', means_str)
    r_re = re.match(r'^\d*\.\d+$', means_str)
    i_re = re.match(r'^\d+$', means_str)
    if p_re:
        mean_ratio = int(p_re.group(1)) / 100.0
        mean_count = int(point_count * mean_ratio)
    elif r_re:
        mean_ratio = float(r_re.group(0))
        mean_count = int(point_count * mean_ratio)
    elif i_re:
        mean_count = int(i_re.group(0))
    else:
        print 'Value of --means argument was malformed'
        exit()
        #mean_count = random.randint(1, point_count / 3)

    return mean_count


def get_options(get_context=False, get_arrays=False, parser=argparse.ArgumentParser()):
    parser.add_argument('-d', '--dim-count', type=int, default=np.random.randint(1, 7),
                        help='Number of dimensions')
    parser.add_argument('-p', '--point-count', type=int, default=np.random.randint(10, 100),
                        help='Number of points')

    #parser.add_argument('-m', '--mean-count', type=int, default=np.random.randint(1, 10), help='Number of means or ratio of means to points')
    parser.add_argument('-m', '--mean-count', type=str, default='30%', help='Number of means or ratio of means to points')

    parser.add_argument('-w', '--workgroup-size', type=int, default=np.random.randint(1, 512), help='Number of workitems in a workgroup (OpenCL).')

    parser.add_argument('--weighted', action='store_true', help='Weighted points')
    parser.add_argument('--log-weighted', action='store_true', help='Weighted points, log scaled weights')

    parser.add_argument('-v', nargs='?', action=VAction, dest='verbose')
    parser.add_argument('--print-input', action='store_true')
    parser.add_argument('--log-steps', action='store_true')

    parser.add_argument('--opencl', action='store_true', help='OpenCL')
    parser.add_argument('--cuda', action='store_true', help='Use CUDA')

    parser.add_argument('--platform', type=int, default=-1, help='OpenCL platform to use')
    parser.add_argument('--devices', type=devices, default=[], help='OpenCL device(s) to use')

    parser.add_argument('--max-steps', type=int, default=-1, help='Maximum number steps done in kmeans calculation')
    #parser.add_argument('--max-steps', type=int, default=-1, help='Stop kmeans when this number of steps has been taken')
    parser.add_argument('--max-delta', type=float, default=1e-6, help='Stop kmeans when means change less than this')  # 0.00001
    parser.add_argument('--check-delta', action='store_true', help='Stop kmeans when delta vanishes')

    parser.add_argument('-P', '--profiling', action='store_true', help='Get profiling on the different parts of kmeans')
    parser.add_argument('--event-history', action='store_true', help='Show overview of event scheduling')

    parser.add_argument('-O', '--out-of-order', action='store_true', help='Enable parallel (out-of-order) execution of queue tasks')
    parser.add_argument('-M', '--multi-queue', action='store_true', help='Use multiple OpenCL queues.')

    parser.add_argument('--loop-unroll', action='store_true', help='Unroll loops over dimensions.')
    parser.add_argument('--use-ternary', action='store_true', help='Use ternary instead of if()')

    parser.add_argument('-F', '--fortran', action='store_true', help='Profiling on Fortran data')
    #parser.add_argument('-F', '--fortran', action='store_true', help='Do a performance comparison with Fortran LBG code.')
    parser.add_argument('--LBG', action='store_true', help='Use Linde-Buzo-Gray k-means algorithm')

    parser.add_argument('-B', '--benchmark', action='store_true', help='Do a performance comparison run.')
    parser.add_argument('-s', '--samples', type=int, default=10, help='Number of samples for benchmark')


    parser.add_argument('-V', '--visual', action='store_true', help='Calculate and display 2D kmeans')


    parser.add_argument('--no-cross-templating', action='store_true', help='Disable templating for cross-compatible (OpenCL/CUDA) kernels')

    parser.add_argument('--no-numpy', action='store_true', help='Disable Numpy kmeans')
    parser.add_argument('--numpy-method', type=int, default=4, help='Cases for handling empty clusters')

    parser.add_argument('--check-result', action='store_true', help='Disable checking of results')


    args = parser.parse_args()

    if os.environ.get('USE_CUDA'):
        args.cuda = True

    if not args.opencl and not args.cuda:
        args.opencl = True
    if args.opencl and args.cuda:
        args.opencl = False
        args.cuda = True

    if args.visual:
        args.dim_count = 2

    # Max steps vs. check delta
    args.max_step_count = args.max_steps
    if args.max_step_count < 0:
        args.check_delta = True
        args.max_step_count = 500

    # Loop unrolling
    if args.loop_unroll:
        args.dimLoopUnroll = True
        args.dims = range(args.dim_count)
        # args.i_dim = 'i_dim'
    else:
        args.dimLoopUnroll = False
        args.dims = ['i_dim']
        args.i_dim = 'i_dim'

    args.mean_count = parse_means(args.mean_count, args.point_count)

    if get_arrays:
        args.points = np.random.rand(args.point_count, args.dim_count).astype(np.float32)
        args.means = np.random.rand(args.mean_count, args.dim_count).astype(np.float32)

    if args.event_history:
        args.profiling = True

    # Inject command line arguments into the global `gpu_lib.options`
    from gpu_lib import options
    options.update(vars(args))

    args.ctx = None
    if get_context:
        if args.devices == []:
            if args.opencl:
                args.ctx = cl.create_some_context()
        else:
            if args.opencl:
                if args.platform == -1:
                    args.platform = 0
                platform = cl.get_platforms()[args.platform]
                args.ctx = cl.Context([platform.get_devices()[i] for i in args.devices])
        return args, vars(args)
    else:
        return args, vars(args)
