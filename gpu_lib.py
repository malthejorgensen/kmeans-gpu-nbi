import os
import time
import numpy as np

import event_history as eh

event_history = eh.EventHistory()

options = {}

if os.environ.get('USE_CUDA'):
    import pycuda.driver as cuda
    from pycuda.compiler import SourceModule
    import pycuda.gpuarray as array
    options['cuda'] = True
else:
    import pyopencl as cl
    import pyopencl.array as array
    options['opencl'] = True


class NullEvent(object):
    def wait(self):
        pass


if 'opencl' in options and options['opencl']:
    # http://www.cse.ohio-state.edu/~agrawal/788-sp13/Papers/5.opencl.pdf
    api_keywords = {
        'kernel': '__kernel',
        'global': '__global',
        'local': '__local',
        'synchronize': 'barrier(CLK_LOCAL_MEM_FENCE);',

        'global_id0': 'get_global_id(0)',
        'global_id1': 'get_global_id(1)',
        'global_size0': 'get_global_size(0)',
        'global_size1': 'get_global_size(1)',
        'group_id0': 'get_group_id(0)',
        'group_id1': 'get_group_id(1)',
        'local_id0': 'get_local_id(0)',
        'local_id1': 'get_local_id(1)',
        'local_size0': 'get_local_size(0)',
        'local_size1': 'get_local_size(1)',
        'workgroup_size0': 'get_local_size(0)',
        'workgroup_size1': 'get_local_size(1)',
    }
elif 'cuda' in options and options['cuda']:
    api_keywords = {
        'kernel': '__global__',
        'global': '',
        'local': '__shared__',
        'synchronize': '__syncthreads();',

        'global_id0': 'blockDim.x * blockIdx.x + threadIdx.x',
        'global_id1': 'blockDim.y * blockIdx.y + threadIdx.y',
        'global_size0': 'blockDim.x * gridDim.x',
        'global_size1': 'blockDim.y * gridDim.y',
        'group_id0': 'blockIdx.x',
        'group_id1': 'blockIdx.y',
        'local_id0': 'threadIdx.x',
        'local_id1': 'threadIdx.y',
        'local_size0': 'blockDim.x',
        'local_size1': 'blockDim.y',
        'workgroup_size0': 'blockDim.x',
        'workgroup_size1': 'blockDim.y',
    }


def compile_kernel(ctx, code, function_name, template_var_dict=None, compile_time=None):

    if template_var_dict is not None:
        from Cheetah.Template import Template as CheetahTemplate
        dictionaries = [template_var_dict]
        print options
        if not ('no_cross_templating' in options and options['no_cross_templating']):
            dictionaries.append(api_keywords)

        code = str(CheetahTemplate(code, searchList=dictionaries))

    if 'verbose' in options and options['verbose']:
        print 'Compiling "%s"...' % function_name
    if 'verbose' in options and options['verbose'] >= 3:
        print code

    kernel = None
    start = time.time()
    if 'opencl' in options and options['opencl']:
        kernel = getattr(cl.Program(ctx, code).build(), function_name)
    elif 'cuda' in options and options['cuda']:
        # http://documen.tician.de/pycuda/driver.html#pycuda.compiler.SourceModule
        arch = None
        c_arch = None
        if options['compute_capability'][0] >= 2:
            c_arch = 'sm_20'
            arch = 'compute_20'

        coptions = ['--compiler-options', '-Wall']
        if 'verbose' in options and options['verbose'] >= 3:
            coptions.append('--ptxas-options=-v')
        module = SourceModule(code, options=coptions, arch=arch, code=c_arch)
        kernel = module.get_function(function_name)
    stop = time.time()

    if compile_time is not None:
        compile_time[function_name] = stop - start

    return kernel


def call_kernel(name, queue, kernels, workgroup_sizes, arg_lists, non_ref_args=[], options={}, wait_for=None):
    return call_kernel_imp(queue, kernels[name],
                               workgroup_sizes[name],
                               arg_lists[name],
                           non_ref_args=non_ref_args, options=options, name=name, wait_for=wait_for)


def call_kernel_imp(queue, kernel, workgroup_size, arg_list, non_ref_args=[], options={}, name=None, wait_for=None):
    if name:
        if 'verbose' in options and options['verbose'] >= 2:
            print name + ':',
            print 'global size: %s' % (workgroup_size[0], ),
            print 'local size: %s' % (workgroup_size[1], )
        if 'verbose' in options and options['verbose'] >= 3:
            print 'Arg list:', arg_list

    args = arg_list + non_ref_args

    if 'opencl' in options and options['opencl']:
        if kernel.num_args != len(args):
            error_str = 'Number of given arguments (%u) not equal to number of kernel arguments (%u)' % \
                            (kernel.num_args, len(args))
            raise Exception(error_str)

        event = kernel(
            queue,
            workgroup_size[0],
            workgroup_size[1],
            *args,
            wait_for=wait_for
        )
        if name is None:
            name = "__unknown__"
        event_history.log(name + '_event',  event)
        return event

    elif 'cuda' in options and options['cuda']:
        block_size = [1, 1, 1]
        for i, wg_size in enumerate(workgroup_size[1]):
            block_size[i] = wg_size

        grid_size = range(len(workgroup_size[0]))
        for i in range(len(workgroup_size[0])):
            grid_size[i] = workgroup_size[0][i] / workgroup_size[1][i]

        kernel(*args, block=tuple(block_size), grid=tuple(grid_size))
        return NullEvent()


def allocate_buffer(ctx, queue, size_or_ndarray, name=None):

    if type(size_or_ndarray) == np.ndarray:
        size = size_or_ndarray.astype(np.float32).nbytes
    elif type(size_or_ndarray) == int:
        size = size_or_ndarray
    else:
        raise Exception("Unknown type:", type(size_or_ndarray))
    print 'Allocating "%s": %.2f MiB' % (name, size / 1024.0**2)

    # Allocate memory and transfer data
    if 'opencl' in options and options['opencl']:
        mf = cl.mem_flags
        if type(size_or_ndarray) == np.ndarray:
            #return cl.Buffer(ctx, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=np.asfortranarray(size_or_ndarray.astype(np.float32)))
            buf = cl.Buffer(ctx, mf.READ_WRITE, size_or_ndarray.astype(np.float32).nbytes)
            cl.enqueue_copy(queue, buf, np.asfortranarray(size_or_ndarray.astype(np.float32))).wait()
            return buf
        elif type(size_or_ndarray) == int:
            return cl.Buffer(ctx, mf.READ_WRITE, size_or_ndarray)
        else:
            raise Exception("Unknown type:", type(size_or_ndarray))

    elif 'cuda' in options and options['cuda']:
        if type(size_or_ndarray) == np.ndarray:
            buf = cuda.mem_alloc(size_or_ndarray.astype(np.float32).nbytes)
            cuda.memcpy_htod(buf, np.asfortranarray(size_or_ndarray.astype(np.float32)))
            #buf = cuda.In(np.asfortranarray(size_or_ndarray.astype(np.float32)))
            return buf
        elif type(size_or_ndarray) == int:
            return cuda.mem_alloc(size)
        else:
            raise Exception("Unknown type:", type(size_or_ndarray))


def transfer_memory(queue, to_buf, from_buf):
    if 'opencl' in options and options['opencl']:
        event = cl.enqueue_copy(queue, to_buf, from_buf)
        return event

    elif 'cuda' in options and options['cuda']:
        if type(to_buf) == np.ndarray:
            cuda.memcpy_dtoh(to_buf, from_buf)
        elif type(from_buf) == np.ndarray:
            cuda.memcpy_htod(to_buf, from_buf)
        else:
            raise Exception('Weird memory transfer')
        return NullEvent()


def avail_mem(mem, platform_name, device_name):
    print platform_name, device_name
    if device_name == 'GeForce 9400M' and platform_name == 'Apple':
        return mem * 0.6
    elif device_name[0:8] == 'Intel(R)' and platform_name == 'Apple':
        return mem * 0.1
        #return 1024 * 1024 * 1
    else:
        return mem
