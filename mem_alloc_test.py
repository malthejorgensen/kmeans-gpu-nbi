import pyopencl as cl
import numpy as np

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--step-size', type=int, default=100, help='How much to increase the buffer size each step (MiB)')
parser.add_argument('--max-mem-size', type=int, default=-1, help='How the maximum buffer size (MiB)')

import cliargs
args, options = cliargs.get_options(parser=parser)

size = args.step_size


ctx = None
if 'ctx' not in options:
    ctx = cl.create_some_context()
else:
    ctx = options['ctx']

queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

import kernels.opencl_kernels as kernels
set_delta_kernel = cl.Program(ctx, kernels.set_delta_code).build()

import event_history as eh
event_history = eh.EventHistory()

steps = 20
if args.max_mem_size > 0:
    steps = args.max_mem_size // size

for i in xrange(steps):
    try:
        nbytes = (i+1) * size * 1024.0**2
        print 'Allocating buffer: %.2f MiB.' % (nbytes / 1024.0**2),
        mf = cl.mem_flags
        buf = cl.Buffer(ctx, mf.READ_WRITE, int(nbytes))
        print 'Buffer created.',
        set_delta_kernel_event = set_delta_kernel.set_delta(queue, (1, ), (1, ), buf, np.float32(np.inf))
        print 'Kernel called.'

        event_history.log('set_delta', set_delta_kernel_event)

        set_delta_kernel_event.wait()

        buf.release()
    except cl.MemoryError:
        print cl.MemoryError
        break
    except cl.RuntimeError:
        print cl.RuntimeError
        break

event_history.save('mem_alloc_events.pickle')
