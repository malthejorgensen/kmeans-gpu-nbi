import numpy as np

colors = ['red', 'blue', 'green', 'purple', 'yellow', 'pink', 'turquoise', 'black', 'maroon', 'orange', 'gray'][::-1]
colors += ['red', 'red', 'red', 'red']

# This cannot be an inner class of PickleableEvent as inner classes cannot be
# readily pickled :(
class Profile(object):

    def __init__(self, event):
        self.queued = event.profile.queued
        self.submit = event.profile.submit
        self.start = event.profile.start
        self.end = event.profile.end

# Event objects cannot be pickled - so we need a replacement.
# Now why don't we just replace them right from the start?
# Answer: Requesting e.g. event.profile.end before the event has actually ended
# results in a runtime error. So we should only request them when we actually
# need to - i.e. on save() and graph()
class PickleableEvent(object):

    def __init__(self, event):
        self.profile = Profile(event)


class EventHistory(object):

    def __init__(self):
        self.events = []

    def log(self, name, event):
        self.events.append((name, event))

    def save(self, filename):
        import pickle
        for i in xrange(len(self.events)):
            self.events[i] = (self.events[i][0], PickleableEvent(self.events[i][1]))
        pickle.dump(self.events, open(filename, 'wb'))

    def load(self, f):
        import pickle
        if type(f) is file:
            self.events = pickle.load(f)
        elif type(f) is str:
            self.events = pickle.load(open(f, 'rb'))

    def print_timings(self):
        event_dict = {}
        for name, event in self.events:
            if name not in event_dict:
                event_dict[name] = 0
            event_dict[name] += event.profile.end - event.profile.start

        event_list = sorted([(time, name) for (name, time) in event_dict.items()])[::-1]

        max_len = max(map(len, event_dict.keys()))
        max10 = max(int(max(map(np.log10, event_dict.values()))) - 9, 0)
        format_str = "%%%us %%%u.2f s" % (max_len, max10)
        for time, name in event_list:
            print format_str % (name, time / 10.0 ** 9)
            #print "%s %.2f s" % (name, time / 10.0 ** 9)

    def graph(self):
        try:
            # http://stackoverflow.com/a/8782324/118608
            import matplotlib.pyplot as plt
            import matplotlib.colors as pltcolors

            fig = plt.figure()
            ax = fig.add_subplot(111)
            #ax.set_aspect(1)

            def avg(a, b):
                return (a + b) / 2.0

            event_type_count = len(list(set([p[0] for p in self.events])))
            colors = np.zeros((event_type_count, 1, 3))
            colors[:, 0, 0] = np.linspace(0.0, 1.0, num=event_type_count, endpoint=False)
            colors[:, :, 1] = 1.0
            colors[:, :, 2] = 1.0
            colors = pltcolors.hsv_to_rgb(colors)[:, 0, :]

            legend_rects = []
            legend_names = []
            legends = {}

            start_time = np.inf
            end_time = -np.inf
            for (event_id, (name, event)) in enumerate(self.events):
                event_queued = event.profile.queued / 10.0**9
                event_submitted = event.profile.submit / 10.0**9
                event_start = event.profile.start / 10.0**9
                event_end = event.profile.end / 10.0**9

                if event_start < start_time:
                    start_time = min(start_time, event_queued, event_submitted, event_start, event_end)
                if event_end < end_time:
                    end_time = max(end_time, event_queued, event_submitted, event_start, event_end)

            max_y = -np.inf
            queue_positions = [-np.inf] # start a single queue at the beginning of time
            for (event_id, (name, event)) in enumerate(self.events):
                event_queued = event.profile.queued / 10.0**9 - start_time
                event_submitted = event.profile.submit / 10.0**9 - start_time
                event_start = event.profile.start / 10.0**9 - start_time
                event_end = event.profile.end / 10.0**9 - start_time
                event_duration = event_end - event_start

                plt.text(event_queued, 0, event_id,
                                        horizontalalignment='center',
                                        verticalalignment='center')
                plt.text(event_submitted, 1, event_id,
                                        horizontalalignment='center',
                                        verticalalignment='center')


                # Events can be reported as starting in the same nanosecond as the
                # previous one ended - this does not imply parallel executed queue
                # items - rather they should be considered sequential
                y = -1
                for i, pos in enumerate(queue_positions):
                    if event_start >= pos: # this event starts later than queue `i`
                        y = i # so we put it in queue `i`
                        break

                # this event starts before all current queue positions
                if y == -1:
                    # so create a new queue
                    queue_positions.append(event_end)
                    # the position of the new queue is of course
                    y = len(queue_positions) - 1

                queue_positions[y] = event_end
                y += 2
                max_y = max(y, max_y)

                if name not in legends:
                    color = colors[-1, :]
                    colors = colors[:-1, :]
                    legends[name] = color
                    legend_rects.append(plt.Rectangle((0, 0), 0, 0, fc=color))
                    legend_names.append(name)
                else:
                    color = legends[name]

                x1 = [event_start, event_end]
                y1 = [y, y]
                y2 = [y+1, y+1]
                plt.fill_between(x1, y1, y2=y2, color=color)
                plt.text(avg(x1[0], x1[1]), avg(y1[0], y2[0]), event_id,
                                        horizontalalignment='center',
                                        verticalalignment='center')
                #plt.text(avg(x1[0], x1[1]), avg(y1[0], y2[0]), name,
                                        #horizontalalignment='center',
                                        #verticalalignment='center')


            plt.legend(legend_rects, legend_names, loc='lower left')

            #ax.axes.get_yaxis().set_visible(False)
            plt.yticks([0, 1, 2], ['Queued', 'Submitted', 'Run'])
            plt.ylim(max_y+1, 0)
            plt.show()
        except ImportError:
            pass

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--file', type=argparse.FileType('rb'), default=open('event_history.pickle', 'rb'),
        help='File to load pickled event history from')

    args = parser.parse_args()

    event_history = EventHistory()
    event_history.load(args.file)

    begin = event_history.events[0][1].profile.start
    for i, (name, event) in enumerate(event_history.events):
        print '%2u %22s %12u %12u' % (i, name, event.profile.start - begin, event.profile.end - begin)

    event_history.graph()
