import pyopencl as cl
import cliargs

def print_platform_info(platform):
    print("===============================================================")
    print("Platform name:", platform.name)
    print("Platform profile:", platform.profile)
    print("Platform vendor:", platform.vendor)
    print("Platform version:", platform.version)
    print("===============================================================")

def print_device_info(device):
    print("---------------------------------------------------------------")
    print("Device name:", device.name)
    print("Device type:", cl.device_type.to_string(device.type))
    print("Device memory: ", device.global_mem_size//1024//1024, 'MB')
    print("Device local memory: ", device.local_mem_size)
    print("Device local memory: ", device.local_mem_type)
    print("Device max clock speed:", device.max_clock_frequency, 'MHz')
    print("Device compute units:", device.max_compute_units)
    print("Device max work group size:", device.max_work_group_size)
    print("Device max work item sizes:", device.max_work_item_sizes)
    print("---------------------------------------------------------------")

    #ctx = cl.Context([device])
    #queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
    #null_kernel = cl.Program(ctx, '__kernel void null(__global float* a) { int b = 2; }').build()
    #local_mem_kernel = cl.Program(ctx, '__kernel void loc(__local float* a) { a[0] = 2; }').build()
    #local_mem_kernel.loc(queue, (1, ), None, cl.LocalMemory(16320)).wait()
    #try:
        #print "Local memory:", cl.Kernel(null_kernel, 'null').get_work_group_info(cl.kernel_work_group_info.LOCAL_MEM_SIZE, device)
    #except:
        #pass
    #try:
        #print "Wavefront-size:", cl.Kernel(null_kernel, 'null').get_work_group_info(cl.kernel_work_group_info.PREFERRED_WORK_GROUP_SIZE_MULTIPLE, device)
        #print "Wavefront-size:",
        #print cl.characterize.get_simd_group_size(device, 4)
        #print cl.characterize.usable_local_mem_size(device)
    #except:
        #pass

if __name__ == '__main__':
    args, options = cliargs.get_options(False)

    for i, platform in enumerate(cl.get_platforms()):
        if (args.platform >= 0 and i == args.platform) or args.platform == -1:
            print_platform_info(platform)
            for i, device in enumerate(platform.get_devices()):
                if args.devices == [] or i in args.devices:
                    print_device_info(device)
