import numpy as np
import numpy
import random
import time


# Lloyd's algorithm
def kmeans(X, M, options={}, empty_cluster_method=4):
    n, d = X.shape
    k, d2 = M.shape
    assert(d == d2)

    assert n * k * 4 < 800 * 1024 ** 2, 'D matrix very large, this will fail'

    tot_time = 0
    mat_time = 0

    # Stopping condition
    dim_ranges = np.max(X, 0) - np.min(X, 0)
    diagonal = np.sqrt(np.sum(dim_ranges ** 2))

    for i in xrange(10000):
        # STEP

        # Calculate distances (squared)
        D = -2 * X.dot(M.T)
        D += np.reshape(np.sum(X**2, 1), (n, 1))
        D += np.reshape(np.sum(M**2, 1), (1, k))

        # The calculation below works if 'X' and 'M' are 'np.mat' not 'np.array'
        #dist_sq = \
            #np.sum(np.power(X, 2), 1) + \
            #np.sum(np.power(M, 2), 1).T - \
            #2 * X * M.T

        # Find the indices of minima
        min_idxs = numpy.argmin(D, 1)

        M_new = numpy.copy(M)

        sort = min_idxs.argsort()
        X = X[:,:][sort]
        cluster_sizes = numpy.bincount(min_idxs)

        # Loop through means
        i = 0
        for j in xrange(len(cluster_sizes)):
            size = cluster_sizes[j]
            if size > 0:
                M_new[j] = numpy.mean(X[i:i+size,:], 0)
                i += size

        # If mean positions didn't change significantly since last
        # iteration - break
        # Again the 'np.delete' is only relevant for solution (2)
        delta = np.abs(M_new - M)
        if np.sum(delta) / k / diagonal < 0.001:
            break
        else:
            M = M_new

    return M_new

# Linde-Buzo-Gray
def lbg(X, k, f=kmeans, epsilon=0.001):
    X = np.mat(X)
    M = np.average(X, 0) # the first mean is simply the average of all points
    _, dims = M.shape

    current_k = 1

    #for i in xrange(int(round(math.log(k, 2))) + 1):
    while True:
        # Find k-means for this set of means
        M = f(X, M)

        # Done?
        if current_k >= k:
            return M
        else:
            # Double the number of means or add the missing number of means
            # whichever is least
            new_rows = min(2*current_k, k) - current_k
            current_k = current_k + new_rows
            # Split each mean randomly
            m_e = M[0:new_rows, :] + np.random.rand(new_rows, dims) * epsilon
            # And add them to the mean array
            M = np.vstack((M, m_e))

if __name__ == "__main__":
    import cliargs
    args, options = cliargs.get_options(get_arrays=True)

    kmeans(args.points, args.means, options)
