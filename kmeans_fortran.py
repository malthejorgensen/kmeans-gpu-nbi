import numpy as np
import fortran.lbgvq_weighted as f_lbg
import fortran.lbgvq_weighted_log as f_lbg_log
import fortran.kmeans_weighted as kmeans_fortran


def kmeans(X, m, options={}, weights=None):
    point_count, dim_count = X.shape

    dim_ranges = np.max(X, 0) - np.min(X, 0)
    diagonal = np.sqrt(np.sum(dim_ranges ** 2))
    options['max_delta'] = diagonal
    if 'LBG' in options and options['LBG']:
        return lbg(X, m.shape[0], options, weights)

    return_weights = True
    if weights is None:
        weights = np.asfortranarray(np.ones(point_count, order='F', dtype=np.float32))
        return_weights = False

    m, m_w = kmeans_fortran.kmeans_weighted(options['max_delta'], X, weights, m)  # dim_count, point_count, mean_count)

    if return_weights:
        return m, m_w
    else:
        return m


def lbg(X, mean_count, options={}, weights=None):
    point_count, dim_count = X.shape
    #mean_count, dim_count2 = m.shape
    #assert(dim_count == dim_count2)
    # assert mean_count == 2**int(np.log2(mean_count)), 'Number of means (mean_count) must be a power of 2'

    m, m_w = None, None
    #m = np.zeros((mean_count, dim_count))
    #m_w = np.zeros((mean_count))  # mean weights
    #if 'weighted' in options and options['weighted']:
    if weights is not None:
        if 'log_weighted' in options and options['log_weighted']:
            m, m_w = f_lbg_log.lbgvq_weighted_log(X, weights, mean_count)  # dim_count, point_count, mean_count)
        else:
            m, m_w = f_lbg.lbgvq_weighted(X, weights, mean_count)  # dim_count, point_count, mean_count)
    else:
        weights = np.asfortranarray( np.ones(point_count, order='F', dtype=np.float32) )
        m, m_w = f_lbg.lbgvq_weighted(X, weights, mean_count)  # dim_count, point_count, mean_count)

    return m

if __name__ == '__main__':
    # print f_lbg.__doc__
    # print f_lbg.lbgvq_weighted.__doc__

    # print f_lbg_log.__doc__
    # print f_lbg_log.lbgvq_weighted_log.__doc__

    import cliargs
    args, options = cliargs.get_options(False)
    point_count = args.point_count
    mean_count = args.mean_count
    dim_count = args.dim_count

    #points = np.random.rand(point_count, dim_count)
    points = np.asfortranarray( np.random.rand(point_count, dim_count).astype(np.float32) )
    means = np.asfortranarray( np.random.rand(mean_count, dim_count).astype(np.float32) )

    import time
    start = time.time()
    if 'LBG' in options and options['LBG']:
        lbg(points, mean_count)
    else:
        kmeans(points, means, options)
    end = time.time()
    print 'Took %.2f s' % (end - start)
