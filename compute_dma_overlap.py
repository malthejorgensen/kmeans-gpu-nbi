import pyopencl as cl
import numpy as np

import time

import cliargs
args, options = cliargs.get_options()

ctx = None
if 'ctx' not in options:
    ctx = cl.create_some_context()
else:
    ctx = options['ctx']

properties = 0
if 'profiling' in options and options['profiling']:
    print 'Enabled profiling.'
    properties |= cl.command_queue_properties.PROFILING_ENABLE

if 'out_of_order' in options and options['out_of_order']:
    properties |= cl.command_queue_properties.OUT_OF_ORDER_EXEC_MODE_ENABLE

queue1 = cl.CommandQueue(ctx, properties=properties)
queue2 = queue1
if 'multi_queue' in options and options['multi_queue']:
    queue2 = cl.CommandQueue(ctx, properties=properties)

print 'Creating %ux%u random matrix.' % (args.point_count, args.point_count),
large_mat = np.random.rand(args.point_count, args.point_count)
print 'Done.'

mf = cl.mem_flags
print 'Allocating buffer: %.2f MiB.' % (large_mat.nbytes / 1024.0**2),
large_buf = cl.Buffer(ctx, mf.READ_WRITE, int(large_mat.nbytes))
print 'Buffer created.'

import kernels.opencl_kernels as kernels
set_delta_kernel = cl.Program(ctx, kernels.set_delta_code).build()

if 'event_history' in options and options['event_history']:
    import event_history as eh
    event_history = eh.EventHistory()

start = time.time()

transfer_to_gpu_event = cl.enqueue_copy(queue1, large_buf, large_mat)

if 'event_history' in options and options['event_history']:
    event_history.log('transfer_to_gpu_event', transfer_to_gpu_event)

tiny_buf = cl.Buffer(ctx, mf.READ_WRITE, int(4))
for i in xrange(20):
    set_delta_kernel_event = set_delta_kernel.set_delta(queue2, (1, ), (1, ), tiny_buf, np.float32(np.inf))

    if 'event_history' in options and options['event_history']:
        event_history.log('set_delta', set_delta_kernel_event)

    set_delta_kernel_event.wait()

transfer_to_gpu_event.wait()

end = time.time()

print 'Took %.2f s' % (end - start)

if 'event_history' in options and options['event_history']:
    event_history.save('compute_dma_overlap.pickle')
