import numpy as np
import voronoi  # https://gist.github.com/1083431
import matplotlib.pyplot as plt


def plot(array, show=True, **kwargs):

    xs = array[:, 0].tolist()
    if type(xs[0]) == list:
        xs = sum(xs, [])
    ys = array[:, 1].tolist()
    if type(ys[0]) == list:
        ys = sum(ys, [])

    plt.plot(xs, ys, **kwargs)

    if show:
        plt.show()


def showPlot(points=None, plotarea=None):

    if points:
        p = points
        d1 = (p[:, 0].max() - p[:, 0].min()) * 0.1
        d2 = (p[:, 1].max() - p[:, 1].min()) * 0.1
        plotarea = [(p[:, 0].min() - d1, p[:, 0].max() + d1), (p[:, 1].min() - d2, p[:, 1].max() + d2)]

    if plotarea:
        plt.xlim(*plotarea[0])
        plt.ylim(*plotarea[1])
    plt.show(block=False)


def savePlot(filename):
    plt.savefig(filename)


def plotPoints(points):
    plot(points, show=False, linestyle='None', marker='o', markerfacecolor='blue', markersize=5)


def plotMeans(means):
    plot(means, show=False, linestyle='None', marker='x', color='red', markerfacecolor='red', markersize=7)


def plotVoronoi(means, axes=None):
    # if axes:
    #     p = points
    #     d1 = (p[:, 0].max() - p[:, 0].min()) * 0.1
    #     d2 = (p[:, 1].max() - p[:, 1].min()) * 0.1
    #     plotarea = [(p[:, 0].min() - d1, p[:, 0].max() + d1), (p[:, 1].min() - d2, p[:, 1].max() + d2)]

    #     plt.xlim(*plotarea[0])
    #     plt.ylim(*plotarea[1])

    # before = plt.axis()
    voronoi.plotVoronoi(means, axes)
    # plt.axis(before)


def plotFull(points, means, show=True, axes=None):
    plotPoints(points)
    plotMeans(means)
    plotVoronoi(means, axes)

    if show:
        pass
        #showPlot()


def clearPlot():
    plt.cla()
