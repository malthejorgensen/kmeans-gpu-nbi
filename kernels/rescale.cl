/*************************************
* Rescale values
*************************************/

__kernel void rescale(__global float* vals,
                      __global float* minimum)
{
  if (get_global_id(0) < {{ point_count }}) {
    vals[get_global_id(0)] = vals[get_global_id(0)] - *minimum + 1.0;
  }
}

/* vim: set filetype=opencl : */
