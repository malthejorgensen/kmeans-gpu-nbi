/*************************************
* Take `log` of every element in array
*************************************/

__kernel void maplog(__global float* vals)
{
  if (get_global_id(0) < {{ point_count }}) {
    vals[get_global_id(0)] = log(vals[get_global_id(0)]);
  }
}

/* vim: set filetype=opencl : */
