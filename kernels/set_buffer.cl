/********************
* Set delta kernel
*
* Set delta flag in memory
********************/

$kernel void set_buffer($global float* buffer, const float value)
{
  buffer[$global_size0 * $global_id1 + $global_id0] = value;
}

/* vim: set filetype=opencl : */
