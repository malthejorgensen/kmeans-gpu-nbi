/*****************
* Reduce an array
*****************/

#define NEUTRAL_ELEMENT $neutral_element
#define REDUCTION_OP(A,B) $reduction_op

$kernel void reduce($global const float* vals,
                    $global float* reduced_vals,
                     unsigned int val_count
                    )
{

  $local float vals_cached[$max_workgroup_size];

  unsigned int id = $global_id0;
  /*unsigned int id = 2 * $group_id0 * $local_size0 + $local_id0;*/

  // Load data
  if (id < val_count) {
    vals_cached[$local_id0] = vals[id];
  } else {
    vals_cached[$local_id0] = NEUTRAL_ELEMENT;
  }
  /*if (id + $local_size0 / 2 < {{ point_count }}) {*/
    /*vals_cached[$local_id0 + $local_size0 / 2] = vals[id + $local_size0 / 2];*/
  /*} else {*/
    /*vals_cached[$local_id0 + $local_size0 / 2] = NEUTRAL_ELEMENT;*/
  /*}*/

  $synchronize

  // Reduce
  for(unsigned int stride = $local_size0 / 2; stride >= 1; stride /= 2) {
    if ($local_id0 < stride) {
      vals_cached[$local_id0] = REDUCTION_OP(vals_cached[$local_id0], vals_cached[$local_id0 + stride]);
      /*vals[$global_id0] = vals_cached[$local_id0];*/
      /*vals_cached[$local_id0] = REDUCTION_OP(vals_cached[$local_id0], (float)-1.0);*/
    }
    $synchronize
  }

  // Save
  reduced_vals[$group_id0] = vals_cached[0];
}

/* vim: set filetype=opencl : */
