


$kernel void dists_min($global const float* points,
                       $global unsigned int* min_idxs,
                       $global const float* means,
                       const unsigned int current_mean_count) {
  unsigned int point_id = $global_id0;

#if $dists_local_mem
  $local float temp_means[$dim_count * $local_mean_count];
#end if

  unsigned int min_idx = 4294967295;
  float current_min = INFINITY; //0x7f800000 __int_as_float(0x7f800000)
  //current_min = 10000.0f; //0x7f800000 __int_as_float(0x7f800000)

  // load coordinates of this point
  /*#*
#if $dimLoopUnroll
  const float point[$dim_count] = {
#for i_dim in $dims
#if i_dim != 0
    ,
#end if
    points[$point_count * $i_dim + point_id]
#end for
  };
#else
*#*/
  float point[$dim_count];
  if (point_id < $point_count) {
#if not $dimLoopUnroll
    for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
      point[$i_dim] = points[$point_count * $i_dim + point_id];
#end for
#if not $dimLoopUnroll
    }
#end if
  }
  /*#*
#end if
*#*/


  // find mean closest to this point
  float dist;
  float temp;
  for (unsigned int mean_id = 0; mean_id < current_mean_count; mean_id++) {

    // Pre-cache some means in local memory
#if $dists_local_mem
    if (mean_id % $local_mean_count == 0) {
      for (unsigned int local_mean_id = $local_id0; local_mean_id < $local_mean_count; local_mean_id += $local_size0) {
#if not $dimLoopUnroll
      for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
        if (mean_id + local_mean_id < current_mean_count) {
          temp_means[$i_dim * $local_mean_count + local_mean_id] = means[$i_dim * $mean_count + mean_id + local_mean_id];
        } else {
          temp_means[$i_dim * $local_mean_count + local_mean_id] = INFINITY;
        }
#end for
#if not $dimLoopUnroll
      }
#end if
    }
  }
  $synchronize
#end if

    // Calculate distance
    dist = 0.0f;
#if not $dimLoopUnroll
    for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
#if $dists_local_mem
      temp = temp_means[$i_dim * $local_mean_count + (mean_id % $local_mean_count)] - point[$i_dim];
#else
      temp = means[$i_dim * $mean_count + mean_id] - point[$i_dim];
#end if
      temp *= temp;
      dist += temp;
#end for
#if not $dimLoopUnroll
    }
#end if

    // Update minimum
#if $use_ternary
    min_idx = (dist < current_min) ? mean_id : min_idx;
    current_min = (dist < current_min) ? dist : current_min;
#else
    if (dist < current_min) {
      min_idx = mean_id;
      current_min = dist;
    }
#end if
  }

  // Save to global memory
  if (point_id < $point_count) {
    min_idxs[point_id] = min_idx;
  }
}

/* vim: set filetype=opencl : */
