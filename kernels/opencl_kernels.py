import os

# http://stackoverflow.com/questions/50499/in-python-how-do-i-get-the-path-and-name-of-the-file-that-is-currently-executin
kernel_path = os.path.dirname(os.path.abspath(__file__))

log_code = open(os.path.join(kernel_path, 'log.cl')).read()
reduce_code = open(os.path.join(kernel_path, 'reduce.cl')).read()
rescale_code = open(os.path.join(kernel_path, 'rescale.cl')).read()

dists_code = open(os.path.join(kernel_path, 'dists.cl')).read()

min1p_code = open(os.path.join(kernel_path, 'min_1_pass.cl')).read()
min2p_code = open(os.path.join(kernel_path, 'min_2_pass.cl')).read()

dists_min_code = open(os.path.join(kernel_path, 'dists_min.cl')).read()

new_means_code = open(os.path.join(kernel_path, 'new_means.cl')).read()

set_buffer_code = open(os.path.join(kernel_path, 'set_buffer.cl')).read()
calc_delta_code = open(os.path.join(kernel_path, 'calc_delta.cl')).read()

split_means_code = open(os.path.join(kernel_path, 'split_means.cl')).read()
