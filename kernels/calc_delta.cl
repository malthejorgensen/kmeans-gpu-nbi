/********************
* Set delta kernel
*
* Set a flag (delta) in memory if some entry in 'm_new' has changed
* significantly from the same entry in 'm'
********************/

$kernel void calc_delta($global const float *m,
                        $global const float *m_new,
                        $global float *is_delta,
                        const unsigned int current_mean_count
             )
{
  if ($global_id0 < current_mean_count) {
    float delta = 0.0f;
#if not $dimLoopUnroll
    for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for $i_dim in $dims
      delta += fabs(m[$i_dim * $mean_count + $global_id0] - m_new[$i_dim * $mean_count + $global_id0]);
#end for
#if not $dimLoopUnroll
    }
#end if
    is_delta[$global_id0] = delta;
  }
}
/* vim: set filetype=opencl : */
