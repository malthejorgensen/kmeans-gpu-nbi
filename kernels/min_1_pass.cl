/***********************************
* First pass minimum reducion kernel
***********************************/

__kernel void mins(__global const float* dists,
                   __global float* mins,
                   __global unsigned int* min_idxs,
                   const unsigned int current_mean_count
                  )
{
  unsigned int point_id = get_global_id(1);

  // If we if we're done already: return
  if(point_id >= $point_count) { return; }

  float current_min = INFINITY;
  unsigned int current_min_idx = -1;

  // Strided loop over the distances to the means from a single point
  for (unsigned int mean_id = get_local_id(0); mean_id < current_mean_count; mean_id += get_local_size(0)) {
    if (dists[point_id * $mean_count + mean_id] < current_min) {
      current_min = dists[point_id * $mean_count + mean_id];
      current_min_idx = mean_id;
    }
  }

  // Save to global memory
  //mins    [get_local_id(0) * $workgroup_size + get_local_id(1)] = current_min;
  //min_idxs[get_local_id(0) * $workgroup_size + get_local_id(1)] = current_min_idx;
  mins    [point_id * $workgroup_size0 + get_local_id(0)] = current_min;
  min_idxs[point_id * $workgroup_size0 + get_local_id(0)] = current_min_idx;
}

/* vim: set filetype=opencl : */
