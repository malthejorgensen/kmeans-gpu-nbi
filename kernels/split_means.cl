/****************************
* LBG means-splitting kernel
****************************/

$kernel void split_means($global float *m,
                         const unsigned int current_mean_count
                        )
{
  if ($global_id0 < current_mean_count && $global_id0 < $mean_count) {
#if not $dimLoopUnroll
    for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for $i_dim in $dims
      m[$i_dim * $mean_count + current_mean_count + $global_id0] = \
           m[$i_dim * $mean_count + $global_id0] + ${lbg_small_number}f;
      /*m[current_mean_count + $i_dim * $mean_count + $global_id1] = 0;*/
#end for
#if not $dimLoopUnroll
    }
#end if
  }
}
/* vim: set filetype=opencl : */

