/*****************************
* Calculate new means kernel *
*****************************/

$define_weighted

\#ifdef WEIGHTED
typedef float weight_t;
\#else
typedef int weight_t;
\#endif

$kernel void new_means_dim($global const float* points,
                           $global const unsigned int* min_idxs,
\#ifdef WEIGHTED
                           $global const float* point_weights,
                           $global float* new_mean_weights,
\#endif
                           $global float* new_means,
                           const unsigned int current_mean_count
                           )
{

  $local float temp_means[$dim_count * $workgroup_size_new_means * $step_size];
  $local weight_t temp_weights[$workgroup_size_new_means * $step_size];

  //int global_id = get_global_id(0);
  int group_id = $group_id1;
  int local_id = $local_id0;

  unsigned int i_step = group_id;

  // Initialize local memory
  for (int mean_id = 0; mean_id < $step_size; mean_id++) {
    temp_weights[local_id*$step_size + mean_id] = 0;
#if not $dimLoopUnroll
    for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
      temp_means[local_id*$step_size*$dim_count + mean_id*$dim_count + $i_dim] = 0;
#end for
#if not $dimLoopUnroll
    }
#end if
  }

  $synchronize

  // Calculate means in the range i_step*step_size..(i_step+1)*step_size
  for (int point_id = local_id; point_id < $point_count; point_id += $workgroup_size0) {

    // Load the id of the mean closest to this point
    unsigned int mean_id = min_idxs[point_id];

    // if the mean closest to the current point (mean_id) is within the range
    if (i_step*$step_size <= mean_id && mean_id < (i_step+1)*$step_size) {
      mean_id -= i_step*$step_size; // Normalize mean_id to local workgroup
\#ifdef WEIGHTED
\#define WEIGHT point_weights[point_id]
\#else
\#define WEIGHT 1
\#endif
      temp_weights[local_id*$step_size + mean_id] += WEIGHT;
      weight_t total_weight = temp_weights[local_id*$step_size + mean_id];
      // Average each dimension
#if not $dimLoopUnroll
      for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
        float delta;
#for i_dim in $dims
        delta = points[$i_dim * $point_count + point_id] - temp_means[local_id*$step_size*$dim_count + mean_id*$dim_count + $i_dim];
        temp_means[local_id*$step_size*$dim_count + mean_id*$dim_count + $i_dim] += delta*WEIGHT / total_weight;
#end for
#if not $dimLoopUnroll
      }
#end if
    }
  }

  // Ensure that all means have been calculated, and saved to local memory
  $synchronize
  //barrier(CLK_GLOBAL_MEM_FENCE);
  //return;

  // Calculate means across work-items and save to global memory
  for (unsigned int mean_id = local_id; mean_id < $step_size; mean_id += $workgroup_size0) {

    weight_t total_weight = 0;

    float current_mean[$dim_count];

#if not $dimLoopUnroll
    for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
      current_mean[$i_dim] = 0.0f;
#end for
#if not $dimLoopUnroll
    }
#end if

    for (unsigned int workitem_id = 0; workitem_id < $workgroup_size0; workitem_id++) {
      weight_t weight = temp_weights[workitem_id*$step_size + mean_id];
      if (weight > 0) {
        total_weight += weight;
        // Average each dimension
#if not $dimLoopUnroll
        for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
          float delta;
#for i_dim in $dims
          delta = temp_means[(workitem_id*$step_size + mean_id) * $dim_count + $i_dim] - current_mean[$i_dim];
          current_mean[$i_dim] += delta * weight / total_weight;
#end for
#if not $dimLoopUnroll
        }
#end if
      }
    }
    // Save to global memory
    if (total_weight > 0 && (i_step*$step_size + mean_id) < $mean_count) { // only update mean if points were assigned to it
\#ifdef WEIGHTED
      new_mean_weights[mean_id] = total_weight;
\#endif
#if not $dimLoopUnroll
      for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
        new_means[$i_dim * $mean_count + (i_step*$step_size + mean_id)] = current_mean[$i_dim];
#end for
#if not $dimLoopUnroll
      }
#end if
    } else {
      // Empty cluster (set to infinity -- in effect remove)
#if not $dimLoopUnroll
      for (int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for i_dim in $dims
        // Comment the line below out to get method (4)
        // new_means[$i_dim * $mean_count + (i_step*$step_size + mean_id)] = HUGE_VALF;
#end for
#if not $dimLoopUnroll
      }
#end if
    }
  }
}

/* vim: set filetype=opencl : */
