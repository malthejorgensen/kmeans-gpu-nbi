/****************************
* Distance calculation kernel
****************************/

$kernel void dists_local_dim($global const float *X
                             , $global const float *m
                             , $global float *dists
                             , $local float* sharedX
                             , const unsigned int current_mean_count
                             )
{
  /*int point_id = get_global_id(0);*/
  unsigned int mean_id = get_global_id(1);
  //int mean_id = ( get_group_id(1) * $workgroup_size + get_local_id(0) );

  // Load points into local memory
#if not $dimLoopUnroll
  for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for $i_dim in $dims
    if ((get_group_id(0) * $workgroup_size1 + get_local_id(1)) < $point_count) {
      sharedX[$workgroup_size1 * $i_dim + get_local_id(1)] = X[$point_count * $i_dim + (get_group_id(0) * $workgroup_size1 + get_local_id(1))];
    } else {
      sharedX[$workgroup_size1 * $i_dim + get_local_id(1)] = 0;
    }
  /*sharedm[get_local_id(0) + i_dim] = m[( get_group_id(1) * $workgroup_size + get_local_id(0) ) * $dim_count + $i_dim];*/
#end for
#if not $dimLoopUnroll
  }
#end if

  barrier(CLK_LOCAL_MEM_FENCE);

  if (mean_id >= current_mean_count) { return; }

  // Load mean into private memory
  float mean[$dim_count];
#if not $dimLoopUnroll
  for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for $i_dim in $dims
    mean[$i_dim] = m[$mean_count * $i_dim + mean_id];
    //mean[i_dim] = m[$mean_count * i_dim + mean_id];
#end for
#if not $dimLoopUnroll
  }
#end if

  // Calculate distances from points to mean
#if $use_consecutive_banks
  for (unsigned int i_i = get_local_id(1); i_i < $workgroup_size1 + get_local_id(1); i_i++) {
    unsigned int i_p = i_i % $workgroup_size1;
#else
  for (unsigned int i_p = 0; i_p < $workgroup_size1; i_p++) {
#end if
    float dist = 0.0;
    float temp;
#if not $dimLoopUnroll
    for (unsigned int i_dim = 0; i_dim < $dim_count; i_dim++) {
#end if
#for $i_dim in $dims
      temp = mean[$i_dim] - sharedX[$workgroup_size1 * $i_dim + i_p];
      //float temp = mean[i_dim] - sharedX[$workgroup_size1 * i_dim + i_p];
      temp *= temp;
      dist += temp;
#end for
#if not $dimLoopUnroll
    }
#end if

    // Save result to global memory
    if ((get_group_id(0) * $workgroup_size1 + i_p) < $point_count) {
      dists[(get_group_id(0) * $workgroup_size1 + i_p) * $mean_count + mean_id] = dist;
    }
  }
}

/* vim: set filetype=opencl : */
