/***************************************
* Second pass minimum reduction kernel *
***************************************/

__kernel void mins(__global float* mins,
                   __global unsigned int* min_idxs
                  )
{
  unsigned int point_id = get_global_id(0);

  // If we if we're done already: return
  if(point_id >= $point_count) { return; }

  float current_min = HUGE_VALF; // This should be +infinity
  int current_min_idx = -1;

  // Loop over minima
  for (unsigned int i = 0; i < $workgroup_size0; i++) {
    if (mins[get_global_id(0) * $workgroup_size0 + i] < current_min) {
      current_min = mins[get_global_id(0) * $workgroup_size0 + i];
      current_min_idx = i;
    }
  }

  // Save to global memory
  mins    [point_id * $workgroup_size0] = current_min;
  min_idxs[point_id * $workgroup_size0] = min_idxs[point_id * $workgroup_size0 + current_min_idx];
}

/* vim: set filetype=opencl : */
