import numpy as np
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--prefix', type=str, default='testdata', help='Where to put the generated testdata.')
args = parser.parse_args()

def flattened(l):
    return reduce(lambda x,y: x+[y] if type(y) != list else x+flattened(y), l,[])

for i, line in enumerate(open("%s/tests.txt" % args.prefix).readlines()[1:]):
    fields = map(eval, line.split())
    point_count = fields[0]
    mean_count = fields[1]
    point_bounds = fields[2]
    mean_bounds = fields[3]
    dims = flattened([map(len, point_bounds), map(len, mean_bounds)])
    # Number of dimensions must be the same
    assert all([dim == dims[0] for dim in dims])
    dim_count = dims[0]

    points_upper = np.array(point_bounds[0])
    points_lower = np.array(point_bounds[1])
    points_range = points_upper - points_lower
    points = points_range * np.random.rand(point_count, dim_count) + points_lower

    means_upper = np.array(mean_bounds[0])
    means_lower = np.array(mean_bounds[1])
    means_range = means_upper - means_lower
    means = means_range * np.random.rand(mean_count, dim_count) + means_lower

    means_file = '%s/test_%u_points.txt' % (args.prefix, i)
    points_file = '%s/test_%u_means.txt' % (args.prefix, i)
    assert not os.path.exists(means_file)
    assert not os.path.exists(points_file)
    np.savetxt(means_file, points)
    np.savetxt(points_file, means)
