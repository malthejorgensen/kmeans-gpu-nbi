import numpy as np
import time

import cliargs


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--prefix', type=str, default='testdata', help='Where to put the generated testdata.')
args, options = cliargs.get_options(parser=parser)

timings = {}

import kmeans_opencl
import kmeans_fortran
import kmeans_numpy

use_functions = ['Numpy']
if 'Numpy' in use_functions:
    assert args.prefix != 'testdata', 'Please don\'t use "testdata" for Numpy'

functions = [
    ('GPU', kmeans_opencl.kmeans),
    ('Fortran', kmeans_fortran.kmeans),
    ('Numpy', kmeans_numpy.kmeans),
]

for i in range(10):

    points = np.genfromtxt("%s/test_%u_points.txt" % (args.prefix, i), np.float32)
    means = np.genfromtxt("%s/test_%u_means.txt" % (args.prefix, i), np.float32)

    for name, f in functions:
        if name not in use_functions:
            continue

        if name not in timings:
            timings[name] = []

        print '############# Start %s #############' % name
        start = time.time()
        f(points, means, options)
        stop = time.time()
        timings[name].append(stop - start)
        print 'Took %.2f s' % (stop - start)
        print '############# End   %s #############' % name

import pickle
f = open('test ' + time.ctime(time.time()) + '.txt', 'w')
pickle.dump(timings, f)
