kmeans-gpu-nbi
==============
This project is an implementation of the k-means and Linde-Buzo-Gray algorithms
in OpenCL and CUDA. The kernels are written as [Cheetah] templates and then
compiled into either OpenCL or CUDA kernels.
The kernels are run via [PyOpenCL] and [PyCUDA]. Furthermore the project
contains reference Numpy and Fortran implementations for benchmarking/comparison.

Running
-------
A simple test run can be done with

    > python benchmark.py

This uses OpenCL.

Running with CUDA can be done with

    > env USE_CUDA=1 python benchmark.py

Prerequisites
-------------

    > pip install -r requirements.txt

Installs the basic dependecies.

The command

    > python benchmark.py --visual

requires `matplotlib`.

And running on CUDA

    > env USE_CUDA=1 python benchmark.py

requires `pycuda`.

Fortran
-------
If you want to benchmark against the Fortran implementation, run the files
`build.sh` and `build_lbg.sh` in the `fortran` folder. This builds Python
modules from the Fortran source that will be run by benchmark.py.

`benchmark.py` will automatically use the Fortran modules when benchmarking.

Scipy
-----
If you want to benchmark against the Scipy implementation of kmeans, simply
install Scipy e.g.

    > pip install scipy

`benchmark.py` will then automatically use Scipy when benchmarking.

Event history
-------------
The flag `--event-history` can be set to log OpenCL events and their timing,
which then will be shown in a timeline diagram:

    > python kmeans_opencl.py --event-history

The event history is saved in the file `event_history.pickle` and can be viewed
at a later time by running:

    > python event_history.py

given that the data has not been overwritten.

License
-------
The kmeans-gpu-nbi project is licensed under the [3-clause BSD License], which
can be found in the `LICENSE.md` file.

The kmeans-gpu-nbi project depends on and uses [Numpy], [Cheetah] and [PyOpenCL].
It can furthermore use [PyCUDA], [Scipy] and [matplotlib] but does not include
code from these libraries. Relevant licenses are found in `LICENSE.md`.

[Numpy]: http://www.numpy.org/
[Scipy]: http://www.scipy.org/
[PyCUDA]: http://documen.tician.de/pycuda/
[PyOpenCL]: http://documen.tician.de/pyopencl/
[Cheetah]: http://www.cheetahtemplate.org/
[matplotlib]: http://matplotlib.org/
[3-clause BSD license]: http://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_.28.22Revised_BSD_License.22.2C_.22New_BSD_License.22.2C_or_.22Modified_BSD_License.22.29
